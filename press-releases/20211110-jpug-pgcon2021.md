---
layout: default
main_title: PostgreSQL Conference Japan 2021にクリアコードが登壇
sub_title: 
type: press
---

<p class='press-release-subtitle-small'>
11/12（金） 16:10-　PGroongaを使って全文検索結果をより良くする方法</p>

<div class="press-release-signature">
  <p class="date">2021年11月10日</p>
  <p>株式会社クリアコード</p>
</div>


株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平）は、PostgreSQL Conference Japan 2021をシルバースポンサーとして支援しています。また、2021年11月12日（金曜日）16時10分より「PGroongaを使って全文検索結果をより良くする方法」と題して、有料のチュートリアルセッションに登壇します。

「PGroonga」をインストールすることで、全言語対応の超高速全文検索機能を簡単に使用できます。他にも「PGroonga」に実装されている様々な機能を使って、検索の精度を上げることができます。例えば、単純な全文検索では表記ゆれを考慮した検索は難しいですが、PGroongaでは、様々な表記ゆれに対応する機能があり、それらを使って検索精度を向上させることができます。今回のセッションでは、 このような検索精度を向上させるための様々な機能とその使い方を紹介します。

チケットをお持ちの方はぜひ16時10分からのセッションにお越しください。ご希望の方には、講演後「Groonga」「PGroonga」ステッカーをお配りします。
好評につきすでにチュートリアルセッションのチケットは完売しておりますが、PGroongaで検索精度を上げる方法について詳しく知りたい方は、講演資料をご確認ください。講演資料は、11月12日に[ククログ]({{ "blog/" | relative_url }})にて公開予定です。


## 講演の概要
* **日時**：2021年11月12日（金）16時10分-17時00分
* **講演タイトル**：PGroongaを使って全文検索結果をより良くする方法
* **講演者プロフィール**:  堀本　泰弘（Groonga/Mroonga/PGroongaメンテナー)

 2017年からPGroongaを含めたGroonga関連プロジェクトのメンテナンス・開発に従事。
 2020年1月PostgreSQユーザ会中国支部主催「第28回 中国地方DB勉強会 in 岡山」にて[『Amazon RDS + Amazon EC2 + ロジカルレプリケーションを使った低コスト高速全文検索』](https://bit.ly/3GpCQMU),2020年11月特定非営利活動法人 日本PostgreSQLユーザ会主催  「PostgreSQL Conference Japan 2020」にて[『PGroonga運用技法 ～PGroongaのWALを放置していませんか？～』](https://bit.ly/3GqxjWr)など、PostgreSQLにおける全文検索について登壇実績がある。


## PostgreSQL Conference Japan 2021概要
* **開催日程**：2021 年 11月 12日（金）10時 - 18時
* **会場**：AP日本橋（東京都） （東京駅より徒歩5分）
* **参加費**：一般参加/5000円	チュートリアル付き参加券/完売　

* **公式サイト**：https://www.postgresql.jp/jpug-pgcon2021


## Groongaについて
PGroongaは、Groongaを使用して全文検索を行います。
Groongaは日本で開発された、オープンソースのカラムストア機能付き全文検索エンジンです。Groongaを使うと全文検索機能付き高性能アプリケーションを開発することができます。

各種のデータベースや、言語などに合わせた派生のアプリケーションも開発されています。
2010年に1.0.0をリリースしてから、着実にユーザを増やし、レストラン検索サイト、医療機関検索サイト、不動産物件情報サイト等で採用されています。


## クリアコードについて
2006年の設立以来、フリーソフトウェアの推進と、そのためのビジネスの継続を社是とし、データ処理事業（Groonga,Fluentd/Fluent Bit,Apache Arrow）とブラウザ関連事業を展開してきました。

Groongaプロジェクトのメンバーとして、Groonga本体及び関連プロジェクトの開発、リリース、コミュニティでのサポートを行っています。

また導入企業に対するGroongaおよび関連プロジェクトを使ったシステムや製品の開発支援を行っています。これら支援の一環としてお客様の要望を実現するためのGroonga本体の機能拡張も行っており、その成果はGroonga本体に取り込み、すべてのGroongaユーザーがその機能を利用できるようにしています。


### 参考URL
【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})

【本プレスリリース】[{{ site.url }}{% link press-releases/20211110-jpug-pgcon2021.md %}]({% link press-releases/20211110-jpug-pgcon2021.md %})

【関連サービス】[{{ site.url }}{% link services/groonga.md %}]({% link services/groonga.md %})

【関連サイト】[Groonga](https://groonga.org/ja/)・[PGroonga](https://pgroonga.github.io/ja/)


### 当リリースに関するお問合せ先
株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com
