---
layout: default
main_title: メール誤送信対策アドイン「FlexConfirmMail」のMicrosoft Outlook版をリリース
sub_title:
type: press
Description: メールクライアント「Thunderbird」向け誤送信防止アドオンとして多くのユーザーに使われている『FlexConfirmMail』をMicrosoft Outlookアドインとして開発しました。
---

<div class="press-release-signature">
  <p class="date">2022年4月27日</p>
  <p>株式会社クリアコード</p>
</div>

株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平）は、メール送信時のヒューマンエラーを極力減らしトラブルを軽減するための様々な機能を提供するメールクライアント向け誤送信防止拡張機能「FlexConfirmMail」をMicrosoft Outlookアドインとしてリリースしたことをお知らせします。

FlexConfirmMailは、クリアコードが開発しているオープンソースの誤送信対策ソリューションです。送信時に宛先情報を分かりやすく確認できるようにすることで、送信ミスに関するトラブルを効果的に予防します。クリアコードでは、2006年からオープンソースのメールクライアントである「Thunderbird」のカスタマイズ・導入支援及びサポートサービスを提供しており、実際に発生したメール誤送信等のセキュリティインシデントの事例をもとに、その解決策となる仕組みを構築してきました。

今回、FlexConfirmMailの新しいラインアップとして、Microsoft Outlook向けのアドインをリリースいたしました。本製品はオープンソースライセンスで配布しており「誰でも自由に使える誤送信対策」として幅広い企業・法人で活用いただけることを期待しています。

<div class="grid-x grid-padding-x">
<figure class="cell medium-6">
  <figcaption>FlexConfirmMailの確認画面</figcaption>
  <a target="_blank" href="{% link press-releases/20220427-flexconfirmmail-outlook/checking.png %}">
    <img alt="FlexConfirmMailの送信確認画面" src="{% link press-releases/20220427-flexconfirmmail-outlook/checking.png %}">
  </a>
</figure>
<figure class="cell medium-6">
  <figcaption>FlexConfirmMailプロジェクトアイコン</figcaption>
  <a target="_blank" href="{% link press-releases/20220427-flexconfirmmail-outlook/icon.png %}">
    <img alt="プロジェクトアイコン" src="{% link press-releases/20220427-flexconfirmmail-outlook/icon.png %}">
  </a>
</figure>
</div>

## FlexConfirmMailの特長

FlexConfirmMailは、お使いのMicrosoft Outlookにアドインとして導入するだけで、簡単に使い始めることができます。

1. ミスを見逃さない確認画面
   * 宛先のアドレスを社内・社外に分けて確認でき、またTo・Cc・Bccの区分も明瞭に分かりやすく表示します。
   * チェックリストをマークする要領で軽快に確認できるので手間がかかりません。
2. 柔軟な設定管理
   * 確認画面の挙動を細かく設定で制御することができます。
   * シンプルながら有効な管理項目の中から、自分のワークフローにあったチェック項目を設定できます。
3. 遅延送信機能
   * FlexConfirmMailはメールを遅延送信する機能を備えています。
   * 実際の送信までにタイムラグを設けることで、途中で送信を取り消すことができます。

## FlexConfirmMailについて

### FlexConfirmMailのダウンロード

* Microsoft Outlook版のインストーラは公式サイトで配布しています。  
  https://www.flexconfirmmail.com/download.html
* インストール方法を含めた利用手順は次のURLから確認いただけます。  
  https://www.flexconfirmmail.com/quickstart.html

### エンタープライズ向けFlexConfirmMail

* FlexConfirmMailの企業利用をご検討の場合、企業での一括導入を円滑に進めることが可能な法人利用パックサービスをご利用いただけます。
* 法人利用パックは、FlexConfirmMailを利用する法人ユーザーを対象に、 FlexConfirmMail通常版から管理機能を強化したエンタープライズ版と導入・技術サポートを提供するものです。
* 法人利用パックは利用ユーザー数に応じた価格で提供しています。詳しくはウェブページをご確認ください。
  https://www.flexconfirmmail.com/enterprise.html

### FlexConfirmMailの機能表

| 項目 | 通常版 | 法人利用パックのエンタープライズ版 |
|---|---|---|
| 送信確認機能 | - 社内／社外での宛先確認 <br> - 添付ファイルの確認<br> - 注意が必要なドメイン<br> - 注意が必要なファイル名<br> - 送信前カウントダウン | - 社内／社外での宛先確認<br> - 添付ファイルの確認<br> - 注意が必要なドメイン <br> - 注意が必要なファイル名<br> - 送信前カウントダウン
| 設定管理機能 | - 設定画面からカスタマイズ可能|- 設定画面からカスタマイズ可能 |
| 多言語サポート | - 日・英・中の３言語に対応 | - 日・英・中の３言語に対応 |
| サポート | ― | **メールによる技術者サポート（10時間まで）** |
| 集中管理機能 | ― | **グループポリシーでの設定集中管理** <br> **共有フォルダでの設定集中管理**|
| その他 | ― | **デジタル署名付きインストーラを提供** |


## クリアコードについて
クリアコードは、 2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。クリアコードの目的は、単に会社を継続していくことではありません。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。この理念は、我々がフリーソフトウェアの開発で学んだことがベースとなっています。



### 参考URL

【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})  
【本プレスリリース】[{{ site.url }}{% link press-releases/20220427-flexconfirmmail-outlook.md %}]({% link press-releases/20220427-flexconfirmmail-outlook.md %})  
【関連ページ】[FlexConfirmMailプロジェクト](https://www.flexconfirmmail.com/index.html)

### 本リリースに関するお問合せ先

株式会社クリアコード  
TEL：04-2907-4726  
メール：info@clear-code.com
