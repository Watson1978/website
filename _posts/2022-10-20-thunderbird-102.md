---
title: Thunderbird 102の法人向け変更点ご紹介
author: piro_or
tags:
- mozilla
---

結城です。

去る2022年10月13日、Thunderbird 102.3.3がリリースされました。
これと同時に、9月20日付けでリリースされたThunderbird 91.13.1のサポートが正式に終了し、自動更新を通じて91.13.1の次のバージョンとしてThunderbird 102が提供されるようになりました。
管理者側で自動更新を停止している場合を除き、すでに更新が適用された環境がほとんどなのではないでしょうか。

この記事では[Thunderbird 102のリリースノート](https://www.thunderbird.net/en-US/thunderbird/102.0/releasenotes/)に記載されている変更点のうち、特に法人利用へ影響がありそうな項目を抜粋して紹介します。
Thunderbird 102の変更の影響度を測りかねてまだ更新を適用できずにいる環境の運用担当者の方に、参考にして頂ければと思います。

<!--more-->

### 「スペースツールバー」の追加

![（Thunderbird 102における、スペースツールバーの様子。ウィンドウ左端の縦型ツールバーと、タブバー左端にボタンとしてまとめられた物のポップアップパネルのそれぞれから、メール・アドレス帳・カレンダー・To Doリスト・チャット・設定画面のそれぞれにアクセスできるようになっている）]({% link /images/blog/thunderbird-102/space-bar.png %} "Thunderbird 102のスペースツールバー")

Thunderbird 91までのバージョンでは、「アドレス帳」「カレンダー」などの組み込みの機能のボタンがタブバー右端に表示されていました。

Thunderbird 102では、それらに代わるUIとしてウィンドウ左端に「スペースツールバー」が追加され、ここからアドレス帳・カレンダー・チャット・設定画面などに即座にアクセスできるようになりました。
閲覧中のメールなどのタブが多数存在している状況でも、目的のタブを探し回らずに済むようになりました。

このUIは、以下のいずれかの方法で非表示に切り替えられます。

* スペースツールバーの一番下の「スペースツールバーを隠します」ボタンをクリックする。
* メニューバーの「表示」→「ツールバー」→「スペースツールバー」のチェックを外す。

これらの方法でスペースツールバーを非表示にした場合は、タブバーの左端にツールバーボタンが表示されるようになり、ここからポップアップでカレンダーなどの機能へ切り替えられるようになります。

管理者側でこのUIの表示・非表示を制御する設定項目は、Thunderbird 102.3.3の時点では存在しません。
また、スペースツールバーを非表示にした場合のタブバー左端のボタンを非表示にすることもできません（この項目はツールバーのカスタマイズの対象外になっています）。
管理者がこのUIを制御するためには、MCD（AutoConfig）に実行可能なコードを記述する形で実現する必要があります。


### アドレス帳UIの刷新

![（Thunderbird 102における、アドレス帳の新UIの様子。アドレス一覧の表示のされ方など、Webアプリのアドレス帳のような外観となっている）]({% link /images/blog/thunderbird-102/new-addressbook-ui.png %} "Thunderbird 102での新UIのアドレス帳")


Thunderbird 91までのバージョンでは、アドレス帳は単独のウィンドウで表示され、ネイティブアプリケーション然としたUIでした。

Thunderbird 102ではアドレス帳のUIが全面的に刷新され、Thunderbirdのウィンドウ内のタブで開かれるようになったのに加え、外観もWebアプリケーション風になりました。

この変更は恒久的な物で、設定などのカスタマイズで従来のUIに戻すことはできません。


### リンクプレビューカードへの対応

![（Thunderbird 102における、リンクプレビューカードが自動挿入された時の様子。文字列のリンクの代わりに、Webサイトが提供する情報がプレビューとして埋め込まれている）]({% link /images/blog/thunderbird-102/link-preview-card.png %} "Thunderbird 102でのリンクプレビューカード")

Thunderbird 102では、HTMLメールの編集画面でメッセージ中にリンクを追加した場合に、Twitterのツイートなどと同様のプレビュー（リンクプレビューカード）を自動で埋め込む機能が追加されました。
このリンクプレビューカードは編集画面上だけでなく、実際に送信するメールにも反映されます。

この機能は初期状態では無効で、以下の方法で制御できます。

* 設定画面の「編集」→「URLの貼り付け時にリンクプレビューを追加する」のチェック状態を変える。
* ポリシー設定の `Preferences` で `mail.compose.add_link_preview` （真偽型）を指定する。（`true` で有効、 `false` で無効）


### 大量の宛先へのメール送信時の再度の警告

![（Thunderbird 102における、大量の宛先へのメール送信を強行しようとした場合の警告ダイアログの様子。「強制送信」あるいは「送信をキャンセル」のいずれかを選択するようになっている。）]({% link /images/blog/thunderbird-102/warn-too-many-public-recipients.png %} "Thunderbird 102での再警告の様子")

Thunderbird 91では、メッセージ作成ウィンドウにおいて大量[^too-many-recipients]の宛先がToまたはCcに列挙されている場合に、Bccへの切り替えが促される仕様でした。

[^too-many-recipients]: こちらの判断の閾値は、初期値では15件となっています。閾値は設定エディターまたはポリシー設定で `mail.compose.warn_public_recipients.threshold`（整数型）を変更することで制御できます。

Thunderbird 102ではその発展として、Bccへの切り替えが選択されなかった場合に、もう一度念のための警告を表示する機能が加わりました。
この機能は初期状態では無効で、以下の方法で制御できます。

* 設定画面の「一般」→ 「設定エディター」で `mail.compose.warn_public_recipients.aggressive` （真偽型）を指定する。（`true` で警告する、 `false` で警告しない）
* ポリシー設定の `Preferences` で `mail.compose.warn_public_recipients.aggressive` （真偽型）を指定する。（`true` で警告する、 `false` で警告しない）

なお、当社ではメール送信前の時点で再確認を促す専用のアドオンとして[FlexConfirmMail](https://addons.thunderbird.net/thunderbird/addon/flex-confirm-mail/)を開発・公開しており、こちらを使うと、宛先の件数以外にも様々な条件で再確認を行えます。

### システムの印刷ダイアログの使用

![（Thunderbird 102における、システムの印刷ダイアログが表示されている様子。）]({% link /images/blog/thunderbird-102/system-print-dialog.png %} "システムの印刷ダイアログ")

Thunderbirdでのメッセージの印刷時には、通常はFirefoxの印刷プレビューと同様のUIが使われます。
OSが提供する印刷ダイアログを使うには、プレビュー画面の右下に表示される「システムダイアログを使用して印刷」をクリックする必要があります。

Thunderbird 102ではこの点について、毎回独自の印刷プレビューを経由せずに、OSの印刷ダイアログを直接表示できるようになりました。
この機能は初期状態では無効で、以下の方法で制御できます。

* 設定画面の「一般」→ 「設定エディター」で `print.prefer_system_dialog` （真偽型）を指定する。（`true` でOSのUIを使用、 `false` で従来の挙動）
* ポリシー設定の `Preferences` で `print.prefer_system_dialog` （真偽型）を指定する。（`true` でOSのUIを使用、 `false` で従来の挙動）


### 暗号化機能の使用の積極的な推奨

![（Thunderbird 102における、メッセージ作成時に暗号化が推奨されている様子。メッセージ編集ウィンドウの最下部に通知バーが表示され、「OpenPGPエンドツーエンド暗号化が可能です。」というメッセージで暗号化を促されている。）]({% link /images/blog/thunderbird-102/notification-for-encryption.png %} "Thunderbird 102での送信メールの暗号化を推奨する通知")

Thunderbird 91までのバージョンでは、仮にメールを暗号化して送れる場面[^can-send-encrypted-message]で暗号化しなかったとしても、特に何も警告は行われませんでした。

[^can-send-encrypted-message]: 具体的には、自分のメールアカウントに紐付くOpenPGP鍵が存在しており、相手のメールアドレスと紐付くOpenPGP公開鍵もインポート済みである場合。

Thunderbird 102ではこのような場面で暗号化をより推奨するようになり、メールを暗号化できる場面であるにも関わらず暗号化しないようにしていると、メール編集画面上にその旨が通知として表示され、暗号化を促すようになりました。

この変更は恒久的な物で、設定などのカスタマイズで従来の動作に戻すことはできません。


### 宛先ごとの送信メール形式を指定する機能の廃止

Thunderbird 91までのバージョンでは、アドレス帳の登録情報を用いて、宛先ごとにHTMLメールを送信するかプレーンテキストのメールを送信するかを切り替えられる仕様でした。

Thunderbird 102ではこの機能が廃止され、設定画面の「編集」配下で全般的な動作として、既定の送信形式を選択・設定する形になりました。

この変更は恒久的な物で、設定などのカスタマイズで従来の動作に戻すことはできません。

### メールアカウントの設定無しでの運用が可能に

![（Thunderbird 102における、メールアカウントのセットアップをキャンセルするかどうかを確認するダイアログの様子。「メールアカウントなしでThundebrirdを使用する」というラベルのチェックボックスが存在している。）]({% link /images/blog/thunderbird-102/use-without-mail-account-dialog.png %} "Thunderbird 102でのメールアカウントの設定キャンセル時の確認ダイアログ")

Thunderbird 91までのバージョンでは、Thunderbirdにメールアカウントの設定が存在しない場合、毎回の起動時に必ずメールアカウントの設定を促すウィザードが表示される仕様でした。
そのため、メールの運用を特に必要としないままThunderbirdを使いたい場合でも、何かしらのダミーのメールアカウントを設定しなくては運用に支障がある状態でした。

Thunderbird 102ではこの制限が撤廃され、メールアカウントの設定を促すウィザードの再表示を抑止できるようになりました。
この機能は初期状態では無効で、アカウントウィザードをキャンセルする際のダイアログでの「メールアカウントなしでThunderbirdを使用する」のチェック状態を変更することで有効化できます。
また、このチェックボックスの状態は以下の方法で制御できます。

* 設定画面の「一般」→ 「設定エディター」で `app.use_without_mail_account` （真偽型）を指定する。（`true` でアカウント不存在時のアカウント作成ウィザードの表示を抑制、 `false` で従来の挙動）

この設定項目はグループポリシーで制御可能な対象には含まれないため、管理者が制御するにはMCD（AutoConfig）を使う必要があります。


### まとめ

Thunderbird 102の変更点のうち、法人での運用に影響があると考えられる代表的な箇所をご紹介しました。

当社では、Thunderbirdの法人運用におけるトラブルの原因究明や解決方法のご提案、要件に基づくカスタマイズ方法の調査、アドオンの開発などを有償にて承っております。
Thunderbirdの運用でお悩みの企業のご担当者さまは、[お問い合わせフォームよりご相談ください]({% link contact/index.md %})。

また当社では、[Thunderbird用アドオン版FlexConfirmMail](https://addons.thunderbird.net/thunderbird/addon/flex-confirm-mail/)と同様の事をMicrosoft Outlookでも実現する[Outlook用アドイン版FlexConfirmMail](https://www.flexconfirmmail.com/)も開発・公開しており、Outlook用アドインの開発も有償にて承っております。
Outlookの法人運用において、特定の運用形態に合わせた機能追加を行うアドインを必要とされている企業のご担当者さまは、上記フォームよりお問い合わせ頂けましたら幸いです。
