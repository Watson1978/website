---
tags:
- groonga
- ruby
- redmine
title: Redmineで高速に全文検索する方法
---
[Redmine](http://www.redmine.org/)で全文検索するとかなり時間がかかります。
<!--more-->


クリアコード社内でもRedmineを使用しており、全文検索が遅いことは以前から問題視していました。

最近Redmineでの全文検索を高速に実行できるようになる[プラグイン](https://github.com/okkez/redmine_full_text_search)を開発したので紹介します[^0]。

リンク先を見てもらえばわかるとおり、PostgreSQLとMySQL(MariaDB)に対応しています。
それぞれ[PGroonga](https://pgroonga.github.io/ja/)と[Mroonga](http://mroonga.org/ja/)を使用しています。

このプラグインを使うと、デフォルトの全文検索と比較して以下のメリットがあります。

  * 高速

    * [Ruby on RailsでPostgreSQLとPGroongaを使って日本語全文検索を実現する方法]({% post_url 2015-11-09-index %}) [PostgreSQLで日本語全文検索 - LIKEとpg_bigmとPGroonga]({% post_url 2015-05-25-index %})

    * [Ruby on RailsでMySQLとMroongaを使って日本語全文検索を実現する方法]({% post_url 2015-11-10-index %}) [MySQLの全文検索に関するあれやこれや](http://www.slideshare.net/yoku0825/mysql-47364986/44)

  * 日付ではなくスコア順に並ぶので関連性が高いものが上に表示されやすい

  * `pgroonga -mroonga`のようにマイナスを付けるとpgroongaは含むがmroongaは含まないというものも検索できる

### インストール方法

プラグインをインストールする前に、使用しているRDBMSに合わせて[PGroonga](https://pgroonga.github.io/install/)または[Mroonga](http://mroonga.org/docs/install.html)をインストールしておいてください。

インストール方法は以下の通り簡単です。PostgreSQLでもMySQLでも同じ方法でセットアップできます。

```text
$ cd redmine/plugins
$ git clone https://github.com/okkez/redmine_full_text_search.git full_text_search
$ cd ../
$ ./bin/rake redmine:plugins:migrate RAILS_ENV=production
```


最後にRedmineを再起動すれば、今までと同じように右上の検索ボックスを使って高速に全文検索できます。

### 他のプラグイン

他には以下のようなプラグインがあります。

  * [Postgresql Full Text Search - Plugins - Redmine](https://www.redmine.org/plugins/redmine_postgresql_search)

    * PostgreSQL で全文検索用のインデックスを追加するプラグイン

  * [Full text searching plugin for Redmine (Elasticsearch) - Plugins - Redmine](https://www.redmine.org/plugins/redmine_elasticsearch)

    * Elasticsearch を使うプラグイン

### まとめ

Redmineで全文検索を高速に実行する方法を紹介しました。

[^0]: Redmine本家にも登録しました <a href="https://www.redmine.org/plugins/full_text_search">https://www.redmine.org/plugins/full_text_search</a>
