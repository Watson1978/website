---
tags: []
title: OSDNのファイルストレージ機能を使ってAPTリポジトリやYUMリポジトリを作成する方法
---
[OSDNのファイルリリース機能をAPI経由で使うには]({% post_url 2016-04-05-index %})という記事では、APTリポジトリやYUMリポジトリをOSDNでは作成できないと紹介していましたが、少し前に[ニュース: rsync, scp, sftpで操作する新ファイルストレージ機能を追加 - OSDN運営・管理 - OSDN](https://osdn.net/projects/sourceforge/news/25629)で、rsyncコマンドでファイルのアップロードができるようになったことを知ったので試してみました。
<!--more-->


[FileStorage_Guide - OSDN ヘルプ - OSDNドキュメント管理 - OSDN](https://osdn.net/docs/FileStorage_Guide)にも使い方が書かれているので参考にしてください。

sourceforge.netに既に構築されているAPTリポジトリやYUMリポジトリをosdn.netにも作ってみました。
既に構築されているリポジトリを対象にするので、APTリポジトリやYUMリポジトリの作成方法については割愛します。

  * https://sourceforge.net/projects/cutter/

  * https://osdn.net/projects/cutter/simple/

まず、sourceforge.net側からAPTリポジトリとYUMリポジトリを丸ごとダウンロードします。
例えば、以下のようなコマンドを実行します。詳細はsourceforge.netのドキュメント等を参照してください。

```
$ rsync -avz --progress --delete username@frs.sourceforge.net:/home/frs/project/c/cu/cutter/debian/ ./debian/
$ rsync -avz --progress --delete username@frs.sourceforge.net:/home/frs/project/c/cu/cutter/centos/ ./centos/
```


Cutterの場合は、debian以下にDebian用のAPTリポジトリ、centos以下にCentOS用のYUMリポジトリを作成していました。

あとは、そのままOSDNにアップロードすればOSDNの豊富なグローバルミラーネットワークでファイルを配布することができます。

Cutterの場合は以下のようにしました。初回の作業だったので手でコマンドを実行しました。

```
$ rsync -avz --progress ./debian/ username@storage.osdn.net:/storage/groups/c/cu/cutter/debian/
$ rsync -avz --progress ./centos/ username@storage.osdn.net:/storage/groups/c/cu/cutter/centos/
```


この場合、APT lineは以下の通りです。

```
deb https://osdn.net/projects/cutter/storage/debian/ stretch main
deb-src https://osdn.net/projects/cutter/storage/debian/ stretch main
```


また、ウェブブラウザから https://osdn.net/projects/cutter/storage/ にアクセスして確認することもできます。

このようにして作成したAPTリポジトリYUMリポジトリ[^0]からパッケージをインストールできることを確認しました。

なお、自動化したスクリプトやウェブサイトの移行については[https://github.com/clear-code/cutter/pull/37](https://github.com/clear-code/cutter/pull/37)で進めています。

[^0]: YUMリポジトリのセットアップはcutter-releaseというパッケージ経由で行うのが楽なので、CentOSユーザーはCutterの次のバージョンがリリースされるのを待つのが良いでしょう
