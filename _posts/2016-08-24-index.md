---
tags:
- presentation
title: '9月1日開催のSpeee Cafe Meetup #2でOSS開発について話します'
---
2016年9月1日に開催される[Speee Cafe Meetup #02](http://speee.connpass.com/event/38159/)で須藤がOSS開発について話します。
<!--more-->


主催の[Speee](http://speee.jp/)さんは開発力向上のために開発者がOSSの開発に参加することを推進しています。クリアコードはそんなSpeeeさんの取り組みをサポートしています。その縁で今回話すことになりました。

OSSの開発に参加するメリットとOSS開発に参加する最初の一歩を支援する取り組みである[OSS Gate](https://oss-gate.doorkeeper.jp/)を紹介する予定です。OSSの開発に興味がある方はぜひお越しください。

OSS Gateについては以下の資料を参考にしてください。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/oss-gate/introduction-japanese/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/oss-gate/introduction-japanese/" title="OSS Gateの紹介">OSS Gateの紹介</a>
  </div>
</div>
