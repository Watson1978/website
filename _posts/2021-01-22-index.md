---
title: フリーソフトウェアプロジェクトを推進するための支援を受けるには
author: kenhys
---

フリーソフトウェアプロジェクトを推進していくにあたって、小規模なうちは問題にならなかった課題に直面することがあります。
例えば、最初は小さなVPSインスタンスで始めたものの、だんだんサーバーリソースが不足しがちになるといった問題です。

多くの人に使われているような著名なプロジェクトであれば、ホスティングに関するスポンサーを探すのは容易でしょうが、そうではないプロジェクトではどうするとよいのでしょうか。
ホスティングの支援を提供しているところでは、GitHubでどれだけスターがついているかを基準にしているところもあります。

個人のプロジェクトとしてWebサービスを始めたものの、増強するとなってくるとややつらい。
そんなときのために支援を受ける方法の1つを紹介します。
<!--more-->

{{site.data.alerts.note}}
> 以下で紹介しているFosshostのサービスは残念ながら終了することが2022年12月にアナウンスされました。
{{site.data.alerts.end}}

### FOSSHOSTとは

FOSSHOSTはオープンソースコミュニティを支援する非営利団体です。
[スポンサー](https://fosshost.org/about)から寄贈されたインフラへのアクセスを無料で提供してくれます。

FOSSHOSTが支援しているプロジェクトのリストは[PROJECTS](https://fosshost.org/projects/)で参照できます。
GNOMEやXubuntuといった著名なプロジェクトを支援していますが、地域のローカルLinuxグループなども支援していることがわかります。

FOSSHOSTが提供可能なリソースは次の通りです。

* [VPSサービス](https://docs.fosshost.org/en/home/virtual-private-server)
* [AArch64サービス](https://docs.fosshost.org/en/home/aarch64-virtual-private-server)
* [ドメインネームサービス](https://docs.fosshost.org/en/home/domain-reg-management)
* [ミラーサービス](https://docs.fosshost.org/en/home/mirrors-as-a-service)
* [メールやウェブサイトのホスティング](https://docs.fosshost.org/en/home/website-hosting)
* [コミュニケーションやコラボレーションのサービス](https://docs.fosshost.org/en/home/communication-collaboration-services)

VPSについては、2021年1月現在、次のリージョンから選ぶことができます。

* メイデンヘッド(イギリス)
* ノッティンガム(イギリス)
* メッペル(オランダ)
* ロサンゼルス(アメリカ)
* シカゴ(アメリカ)
* ダラス(アメリカ)
* オレゴン(アメリカ)

### FOSSHOSTに応募するには


[APPLY](https://fosshost.org/apply)から応募できます。


申請にあたって必要な項目は次のとおりです。

* [Application Form Guidance and Notes](https://docs.fosshost.org/en/home/application)を読んで受諾すること
* 申請者名
* プロジェクトのメールアドレス
* プロジェクトの名前
* プロジェクトにおける役割
* プロジェクトのWebサイトのURLもしくはGitリポジトリのURL
* プロジェクトの説明
* 申請する対象のサービス

応募してしばらくすると、admin@fosshost.orgからメールが届くはずなので、その後の指示(PGPで署名したメールを送るとか、追加の情報を提供するとか)にしたがいます。
申請が受理されると、必要なサービスにアクセスするためのメールが送られてきます。

### まとめ

今回はフリーソフトウェアを推進するための支援を受けたいときに活用できそうな方法を紹介しました。
選択肢の1つとして参考にしてみてください。
