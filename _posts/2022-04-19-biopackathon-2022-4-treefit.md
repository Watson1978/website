---
title: 'Bio"Pack"athon 2022 #4 - Treefitのパッケージング'
author: kou
tags:
  - presentation
---

[Bio"Pack"athon 2022 #4](https://biopackathon.connpass.com/event/230811/)で「Treefitのパッケージング」という話をした須藤です。Treefitとはなにかも簡単に説明しつつTreefitのパッケージングまわりのことを説明します。

<!--more-->

<div class="youtube">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/reMzncPBUNc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/biopackathon-2022-4/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; box-sizing: content-box; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/biopackathon-2022-4/" title="Treefitのパッケージング">Treefitのパッケージング</a>
  </div>
</div>


関連リンク：

  * [動画（YouTube）](https://www.youtube.com/watch?v=reMzncPBUNc)

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/biopackathon-2022-4/)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-biopackathon-2022-4)

### Treefit

[Treefit](https://hayamizu-lab.github.io/treefit/)は[早水先生](https://twitter.com/hayamizu_lab/)らが開発した細胞分化の軌跡推定をするために使えるソフトウェアです。私もTreefitの開発に参加していてR版・Python版パッケージの開発を担当しました。実は、最初のバージョンは2-4年前に開発していて、リリースした頃に[統計数理研究所のプロジェクト紹介](https://www.ism.ac.jp/ism_info_j/labo/project/146.html)[^ism]で記事にしてもらったりもしていたのですがこのブログでは紹介していませんでした。（書くのが面倒くさくて。。。）

[^ism]: 早水先生は今は早稲田大学に所属していますが、当時は統計数理研究所に所属していたのです。

細胞分化の軌跡推定についての詳細は前述のプロジェクト紹介ページや↓の同じイベントでの早水先生の動画をみてください。なんですが、ここでも簡単に紹介しておきます。

動物細胞の場合、幹細胞の分化の軌跡の全体像はツリー構造になるという仮説が古くからありますが、そのツリー構造の全体像にはまだ解明されていない部分が色々と残っています。そこで近年では、最新の実験計測技術が生み出す1細胞の遺伝子発現データという高精細なデータから細胞分化のツリーを再構築する研究に研究者たちの関心が集まっています。このデータ解析課題は「細胞分化の軌跡推定」と呼ばれるもので、これまでに多くの研究者たちが1細胞の遺伝子発現データからツリーを作る手法を提案してきました。しかし、既存手法は細胞分化の軌跡を表すツリーを推定することだけに焦点を合わせており、データから推定されたツリーがどれだけ信頼できるか（もとのデータ点の分布がどれほどツリーっぽいか）を客観的に判断するための方法はありませんでした。Treefitは、データとツリーのフィッティングの良し悪しを測ることで、データから推定されたツリーの信頼性を客観的に評価することを可能にする革新的なツールです。データとツリーの当てはまりの良さを測るという新しい着眼点で開発されたTreefitは既存の手法と競合するものではなく、むしろ推定結果の妥当性を事後評価する目的で様々なツリー推定アルゴリズムと組み合わせて使うことができます。

<div class="youtube">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/1cGpTtrNPWM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

### Treefitのパッケージング

そんなすごいTreefitですが、作っただけで終わりにするのではなく広く使ってもらってさらなる応用研究に活用してもらいたいです。そのためには、関連研究で使われているツールから簡単に使える環境を整備しなくてはいけません。ということで、関連研究をしている人たちによく使われていそうなRとPython用のパッケージを開発しました。

  * R用パッケージ： https://cran.r-project.org/package=treefit
  * Python用パッケージ： https://pypi.org/project/treefit/

Bio"Pack"athonは「バイオのソフトウェアパッケージを作ることを推進するコミュニティ」ということだったので、Treefitのパッケージを作ったときに得られた知見のうち、パッケージを作るとき一般に有用そうな知見を紹介しました。大きく分けて次のことについて紹介しました。

  * ドキュメント関連
  * テスト関連
  * リリース関連

#### ドキュメント関連

広く使ってもらうにあたりドキュメントの整備は非常に重要です。適切なドキュメントがないとユーザー候補はそもそも自分が使うべきパッケージかどうかを判断できません。

よって、「ユーザー候補」が最初に読むべきドキュメントを準備します。「チュートリアル」や「Getting started」と呼ばれている内容のドキュメントになります。R版・Python版でそれぞれ次のようになっていて、文章による説明だけではなくサンプルコードとその実行結果および可視化結果をたくさん載せています。

  * R版チュートリアル： https://hayamizu-lab.github.io/treefit-r/1.0.2/articles/treefit.html
  * Python版チュートリアル： https://hayamizu-lab.github.io/treefit-python/1.0.0/getting_started.html

サンプルコードとその実行結果および可視化結果があることにより直感的に理解しやすくなります。「ユーザー候補」はまだTreefitに対する知識が少ないのでこのようなアプローチの方が理解しやすくなります。

APIはできるだけ簡単に使えるように設計します。よくあるユースケースであればチュートリアルで使っているコードをベースにちょっとパラメーターをカスタマイズすれば使えるようなイメージです。通常のAPIだけでなく高レベルなAPIも用意してよくあるユースケースは高レベルなAPIでカバーするというような設計もアリです。

ただし、便利APIだけではカバーできないユースケースもあるのでそのあたりをサポートするかどうかの設計判断が必要です。初期のリリースでは小さめのAPIにしてユーザーからの要望に応じて徐々に提供するAPIを増やしていくのが現実的です。

そのようにパッケージを成長させていくためには、現時点でどのようなAPIが提供されていてなにができるのか（なにができないのか）をユーザが確認できる状態にしないといけません。そのためのドキュメントが「リファレンス」と呼ばれているものです。

リファレンスで最も重要なのは網羅性です。使えるAPIをすべて網羅していることが最低ラインです。以下はTreefitのリファレンスです。現時点のTreefitはまだAPIが少ないですが、すべて網羅しています。

  * R版リファレンス： https://hayamizu-lab.github.io/treefit-r/1.0.2/reference/index.html
  * Python版リファレンス： https://hayamizu-lab.github.io/treefit-python/1.0.0/api.html

各APIの説明にはできるだけサンプルコードとその実行結果をつけます。チュートリアルと同じように文章だけの説明よりサンプルコードとその実行結果があった方が理解しやすいからです。R版で使っている[pkgdown](https://pkgdown.r-lib.org/)ではHTML生成時にサンプルコードを実行して可視化したものも埋め込めるので図も入っています。Python版で使っている[Sphinx](https://www.sphinx-doc.org/)でも頑張ればできそうな気がしますが、デフォルトではできないので図は入っていません。

  * R版リファレンス例： https://hayamizu-lab.github.io/treefit-r/1.0.2/reference/plot.treefit.html
  * Python版リファレンス例： https://hayamizu-lab.github.io/treefit-python/1.0.0/api/treefit.plot.html

#### テスト関連

Treefitは1回リリースして終わりのプロジェクトではありません。今もより洗練されたアルゴリズムを開発しています。継続的に開発するにあたり自動でテストを実行できる環境を整備しておくことは重要です。エンバグしたことに気づくこともできますし、RやPythonのバージョンアップで壊れていないか確認することにも使えます。

TreefitのリポジトリーはGitHubにあるのでGitHub Actionsを使ってコミットごとにテストを実行しています。テストは既存のテスティングフレームワークを使って書いています。Rでは[testthat](https://testthat.r-lib.org/)、Pythonでは[pytest](https://pytest.org/)を使っています。

Rのパッケージは[CRAN](https://cran.r-project.org/)に登録することになりますが、[CRAN Repository Policy](https://cran.r-project.org/web/packages/policies.html)にある通り、CRANに登録するには少なくとも2つ以上のメジャーなRのプラットフォームで動かないといけません[^cran-2-major-platforms]。GitHub ActionsではUbuntu・macOS・Windowsでテストを実行でき、この条件を満たしているか動作確認しやすいです。

[^cran-2-major-platforms]: "Packages will not normally be accepted that do not run on at least two of the major R platforms."

ただ、一通り動く状態にするまではそれなりに大変（特にドキュメント生成まわりが大変）なのでうまく動かせない人は[R版TreefitのGitHub Actionsの設定](https://github.com/hayamizu-lab/treefit-r/blob/master/.github/workflows/check.yml)を参考にしてください。

#### リリース関連

ユーザーに簡単に使ってもらうためにはパッケージをリリースしないといけません。

Pythonのパッケージは`twine upload`するだけなので簡単です。パッケージの公開にあたりレビューがないからです。

一方、Rのパッケージはレビューがあります。そのため、Pythonパッケージのリリースよりかなり難しいです。[devtools](https://devtools.r-lib.org/)などRのパッケージリリースを支援する各種ツールがあるのでそれを使うことになります。また、前述の[CRAN Repository Policy](https://cran.r-project.org/web/packages/policies.html)を確認することも忘れてはいけません。

ツールが揃っているのですが、ツールの使い方を忘れては使いこなせないのでR版のTreefitでは[ツールを使う各種作業を定型化](https://github.com/hayamizu-lab/treefit-r/blob/master/Rakefile)しています[^rakefile]。

[^rakefile]: RubyではなくRを使うべきだとは思うのですが、私がRubyに慣れているのでついついRubyを使ってしまうのです。。。

### まとめ

Treefitのパッケージ関連の知見はおもしろかったですか？

このようにTreefitのパッケージを開発・メンテナンスしています。参考になる部分があればぜひマネをしてください。R版もPython版もフリーソフトウェアとして自由に再利用できます。

Treefit自体もおもしろいので興味がある人は前述の早水先生の動画やプロジェクト紹介記事もみてください。Treefitの研究・開発に興味がある人は[早水先生](https://twitter.com/hayamizu_lab/)に連絡してください！一緒に研究・開発しましょう！

クリアコードはフリーソフトウェアを開発することに特化したソフトウェア開発会社です。私が最初に早水先生からTreefitの開発の話を聞いたとき、一番おもしろいなーと思ったのは「生物の研究の世界ではオープンソースソフトウェアとして有用なソフトウェアを公開することが論文でもプラスになる」という話でした。そのときは[Monocle](http://cole-trapnell-lab.github.io/monocle-release/)を例として教えてもらいました。クリアコードはフリーソフトウェアを開発する会社なので、そういう研究の世界の人たちをフリーソフトウェアを開発することで支援できたらいいなぁと思ったのです。自分たちの研究に関するソフトウェアの開発も手伝って欲しいという方は[お問い合わせフォーム]({% link contact/index.md %})からご連絡ください。

問い合わせする前に勘違いしているとアレなので念のため説明しますが、フリーソフトウェアの開発は無償ではなく有償です。ここでいうフリーソフトウェアは[GNUの言うフリーソフトウェア](https://www.gnu.org/philosophy/free-sw.html)のことなので無償ということではなく自由ということです。Treefitも有償で開発しました。
