---
title: Firefox ESR91.2のリリースとFirefox ESR78のサポート終了について
author: piro_or
tags:
- mozilla
---

来る11月2日に、Firefoxの法人向け長期サポート版であるFirefox ESR[^esr]の、1つ前のメジャーバージョンにあたる「Firefox ESR78」のサポートが終了します。

[^esr]: Extended Support Release。通例、1年間のセキュリティアップデートが提供され、その間は機能的な変更は行われない。

Firefox ESRは現在、ESR91とESR78の2つのバージョンが存在しており、10月5日には、それぞれのセキュリティアップデート版であるESR91.2とESR78.15がリリースされる見込みです。
このうちESR78.15はESR78の最終バージョンになる予定で、ESR91.2のリリースをもってESR78はサポートが終了する旨予告されており、以後はESR91への移行が強く推奨されています。

当社では、[Firefox ESR78からESR91の間の変更点のレポートを公開しており]({% link downloads/firefox-esr78-esr91-migration-report.md %})、このレポート中では大きく分けて11点の変更を詳細に紹介しています。
この記事ではその中で特に、日本での法人利用に影響が出ると考えられるものを4点抜粋してご紹介します。

<!--more-->

### Firefox ESR78からESR91の間の変更点（抜粋）

#### パスワードマネージャ、Cookie、プライバシー情報の保護に関わる変更

* パスワードマネージャにおいて、保存済みのすべてのログイン情報を 1 クリックで消去できるようになりました。 (Firefox 85)
* 「スーパークッキー」によるユーザートラッキングからユーザーを保護するようになりました。 (Firefox 85)
10
  * 「スーパークッキー」とは、DNS キャッシュ、HSTS、デバイス ID などのさまざまな方法を利用して、Cookie に相当するユーザー識別情報を得る技術です。
  * 従来の動作に戻すには、`privacy.partition.network_state` を `false` に設定します。
* 強化型トラッキング防止機能の厳格モードで「トータル Cookie 保護」が有効になりました。 (Firefox 86)
  * 「トータル Cookie 保護」とは、サイトごとに Cookie を分離する技術です。
  * 通常モードでもサイトごとの分離を有効にするには、`network.cookie.cookieBehavior` を `5` に設定します。
* HTTP リファラの既定の送信ポリシーを変更し、URL のパス部分とクエリ文字列を除去した状態で送出するようにしました。 (Firefox 87)
  * 従来の動作に戻すには、`network.http.referer.defaultPolicy` を `3` を設定します。
* プライベートブラウジングモードにおいて、クロスサイト Cookie を初期状態でブロックするようになりました。 (Firefox 89)

#### セキュリティに関わる変更

* HTTPS から HTTP にフォームを送信する時の警告を抑止できるようになりました。(Firefox 80)
  * 抑止を有効化するには、`security.warn_submit_secure_to_insecure` を `true` に設定します。
* HTTPS で通信する Web サイトのみを閲覧可能にする、HTTPS オンリーモードが追
加されました。 (Firefox 83)
  * 全体で有効化するには、`dom.security.https_only_mode` を `true` に設定します。
  * プライベートウィンドウのみ有効化するには、`dom.security.https_only_mode_pbm` を `true` に設定します。
* TLS クライアント証明書を使っている組織において、Google Chrome 互換の CORS プロトコルの取り扱いが可能になりました。 (Firefox 87)
  * 有効化するには `network.cors_preflight.allow_client_cert` を `true` に設定します。

#### レガシーな仕様への対応終了、機能の廃止に関わる変更

* Adobe Flash サポートが廃止されました。 (Firefox 85)
  * 機能自体が廃止されているため、設定による再有効化はできません。
* FTP サポートが廃止されました。 (Firefox 88/90)
  * 代替手段として FTP を処理するアドオンを作成できるようになりましたが、現時点では実装が存在しません。
  * 引き続き FTP を利用する場合は、FTP を処理するアドオンを新たに開発する必要があります。

#### Web アプリの動作の互換性に影響する変更

* `<a>` および `<area>` 要素で、`target="_blank"` を設定すると `rel="noopener"` が標準で付与されるようになりました。 (Firefox 79)
* `Windows.open()` で非標準の `outerHeight` と `outerWidth` を指定できなくなりました。 (Firefox80)
* サンドボックスが有効化された `<iframe>` で、自動ダウンロードをブロックするようになりました。(Firefox 81)
  * 従来の挙動に戻すには、`dom.block_download_in_sandboxed_iframes` を `false` に設定します。
* `<iframe>` から、非標準の `mozallowfullscreen` 属性が削除されました。 (Firefox 81)
* CSS の非標準の機能の一部が廃止・無効化されました。 (Firefox 81～88)
* `window.name` が、Web サイトをまたいだページ遷移によっても自動消去されるようになりました。 (Firefox 88)
  * 従来の挙動に戻すには、`window.name.update.enabled` を `false` に設定します。
* `localhost` ドメインを、常に `127.0.0.1/::1` として扱うようになりました。 (Firefox 84)
  * ループバック以外のアドレス解決を許可するには、`network.proxy.allow_hijacking_localhost` を `true` に設定します。
* Application Cache API が廃止されました。代わりに Service Worker API を使用する事が推奨されます。 (Firefox 84)
* ARIA のアクセシビリティ属性（`aria-labeledby`、`aria-describedby`）の値の変更をイベントで通知するようになりました。 (Firefox 87)
* `DeviceProximityEvent`、`UserProximityEvent`、`DeviceLightEvent` が廃止されました。(Firefox 89)

### まとめ

[既に公開中のFirefox ESR78からESR91の間の変更点のレポート]({% link downloads/firefox-esr78-esr91-migration-report.md %})から、日本での法人利用に影響が大きいと思われる話題を抜粋してご紹介しました。

EdgeやChromeなど様々な選択肢のブラウザがある中で、集中管理やカスタマイズの利便性、企業向けの長期サポート版（ESR版）のあるFirefoxブラウザは、さまざまな業種の法人での使用実績および支持があります。
当社では、Firefox ESRの旧版から新版への移行（更新）をスムーズにするメタインストーラのご提供や、リモート環境などで課題になりやすいメモリ使用量削減のカスタマイズなどにも対応可能な[サポートサービス]({% link services/mozilla/menu.html %})をご提供しています。
Firefoxの法人利用に際して技術的な詳細の問い合わせ先をお探しのご担当者さまは、[是非当社までお問い合わせ下さい]({% link contact/index.md %})。

