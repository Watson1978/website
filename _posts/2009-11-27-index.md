---
tags:
- test
title: FSIJ月例会の資料公開
---
先日開催された[FSIJ月例会]({% post_url 2009-11-17-index %})でテストとテスティングフレームワークについて話しました。声をかけてくれた上野さん、ありがとうございます。
<!--more-->


[![テスティングフレームワークに必要なもの - 書きやすさとデバッグのしやすさ]({{ "/images/blog/20091127_0.png" | relative_url }} "テスティングフレームワークに必要なもの - 書きやすさとデバッグのしやすさ")](/archives/fsij-200911/)

### 内容

これまでの経験から感じていることをまとめたものになっています。どうにかしたいけど、まだよいアイディアがなくて悩んでいる、といったものも含まれています。例えば、[違いをわかりやすく示すことはいろいろ工夫できるけど、なかったことをわかりやすく示すことは難しい](/archives/fsij-200911/testing-framework-82.html)といったことです。

最後まで話しつづけるのではなく、随時ディスカッションをはさむ形で進みました。参加者のみなさんからもたくさんの意見がでてとても有意義でした。

GUIや[DTP](https://ja.wikipedia.org/wiki/DTP)、ネットワーク・ハードウェア周辺などテストを自動化することが難しい領域のテストはみなさん悩んでいるところでした。今回の会で決定的な解決策がでたわけではないのですが、悩んでいる部分、他の人が持っているよいアイディアを共有できたことは成果と言えます。明日から劇的に変わるというものではありませんが、確実によくなっていけるというものです。

話の中に何度かでていますが、「完璧を求めない」ことが重要です。完璧を求めてテストがストレスになってしまうことよりも、自分たちのペースでよい状態を継続し、よいソフトウェアの開発を続けられるようにすることの方が重要です。このあたりの力の入れ具合、テストが目的ではなくよいソフトウェアを開発することが目的であることを忘れないことで「壁」を越えることができるでしょう。

### あわせて読みたい

  * [iUnitTestでやった事 - リンゴの水やり？(はてな)](http://d.hatena.ne.jp/gutskun/20091126/1259173258)
  * [UxUを用いたデータ駆動テストの記述 - ククログ(2009-10-30)]({% post_url 2009-10-30-index %})
  * [当日の様子を録画したもの](http://hikaru.fsij.org/monthly-meetings/)（フォーマットは[Ogg](https://ja.wikipedia.org/wiki/Ogg))
