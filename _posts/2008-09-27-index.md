---
tags:
- ruby
title: rcairo 1.8.0リリース
---
2008/09/26にマルチプラットフォームで動作するベクトルベースの
グラフィックライブラリである[cairo](https://ja.wikipedia.org/wiki/cairo)
1.8.0がリリースされました。また、同日のうちにcairoを
[Ruby](https://ja.wikipedia.org/wiki/Ruby)から利用するためのライブラリ
[rcairo](http://rubyforge.org/projects/cairo/) 1.8.0が
リリースされました。（[[ruby-list45520] [ANN] rcairo
1.8.0](http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-list/45520)
）rcairo 1.8.0はcairo 1.8.0に対応しています。rcairoの基本的
な使い方は[るびまの記事「cairo: 2 次元画像描画ライブラリ」](http://jp.rubyist.net/magazine/?0019-cairo)にまとまっ
ています。
<!--more-->


ちなみに、誰も気づいていないかもしれませんが、cairoの最新API
への対応は各種言語バインディングの中ではrcairoが最速です。

### cairo 1.8.0

cairo 1.8.0ではテキストの扱いが改良されています。例えば、テキ
ストを検索、選択、コピペできるようなPDFを出力するようになって
います。また、「ユーザフォント」という機能が導入されています。

「ユーザフォント」はその名の通り、ユーザ（cairoを使う開発者）
独自のフォントを定義・利用できる機能です。利用例として、SVGフォ
ントやFlashフォントなど標準化されていないフォーマットのフォン
トの実装があげられています。通常は利用することはないと思いま
すが、cairoがより広く利用される機会を増やす機能になるかもし
れません。

もちろん、rcairoでは「ユーザフォント」もサポートしています。

### rcairoの今後

rcairoには、cairoのバインディングだけではなく、rcairo独自の
cairoをもっと便利に使うための機能が追加されています。例えば、
[Cairo::Color](http://cairo.rubyforge.org/doc/ja/cairo-color.html)
もそのひとつです。

rcairoをもっと便利に使えるようにするためには、レンダリングエ
ンジンのような機能が必要だと考えています。例えば、以下のよう
にすれば表を描画できるというような機能を考えています。

{% raw %}
```ruby
table = Cairo::Table.new(:width => 400)
table << Cairo::Table::Header.new("column1", "column2", "column3")
table << Cairo::Table::Row.new("1x1 value", "1x2 value", "1x3 value")
table << Cairo::Table::Row.new("2x1 value", "2x2 value", "2x3 value")
context.show_table(table)
```
{% endraw %}

現在考えているサポートしたい描画対象は以下の通りです。もし興
味があったらぜひ手伝ってください。

  * 表
    （[PDF::Writer](http://ruby-pdf.rubyforge.org/pdf-writer/index.html)
    が参考になる？）
  * グラフ
    （[Pycairo](http://www.cairographics.org/pycairo/)
    を使っている
    [CairoPlot](https://launchpad.net/cairoplot)（[スクリーンショットあり](http://linil.wordpress.com/2008/09/16/cairoplot-11/)
    ）が参考になる？）
  * QRコード（[rqr](http://rqr.rubyforge.org/)が参考
    になる？）
