---
tags: []
title: Systemdのサービスの依存関係を調べる方法
---
### はじめに

昨今、大抵のLinuxディストリビューションにおいては、Systemdが標準採用されています。
ディストリビューションによって提供されているパッケージを使うだけなら、（通常はすでに適切に設定済みのため）普段それほどサービスの依存関係を意識することはありません。
しかし、独自に開発したソフトウェアをサービスとして動かしたりするときには、サービスの依存関係を正しく指定しないと意図したように動作しないという問題に遭遇することがあります。
<!--more-->


今回はそんなときに便利なサービスの依存関係を調べる方法を紹介します。

### 実際のサービスの起動順序を確認するには

まず、意図した順序でサービスが起動しているか調べるには、`systemd-analyze`コマンドを使います。

`systemd-analyze`にオプションを何も指定しないと、次のように起動にどれだけかかったかを表示します。[^0]

```sh
% systemd-analyze
Startup finished in 14.348s (kernel) + 7.952s (userspace) = 22.300s
```


これに`plot`オプションをつけて実行すると、SVG形式の結果を得られます。
適当なファイルにリダイレクトしてブラウザで表示すると良いでしょう。

```sh
% systemd-analyze plot > something.svg
% firefox something.svg
```


実際に描画してみた結果は次のようになりました。

![systemd-analyze plotの結果(の一部)]({{ "/images/blog/20151228_0.png" | relative_url }} "systemd-analyze plotの結果(の一部)")

凡例については次の通りになっています。

![systemd-analyze plotの凡例]({{ "/images/blog/20151228_1.png" | relative_url }} "systemd-analyze plotの凡例")

このように、どの順序でサービスが起動されたのか、またどれくらい時間がかかっているのかがわかりやすく表示されます。
サービスのunitファイルの記述に漏れがあり、意図しないタイミングで起動されてしまっていたケースなどではこれがヒントになります。

### サービスの依存関係を調べるには

今度は、unitファイルの記述をもとにサービスの依存関係を調べてみましょう。

`systemctl`には`list-dependencies`というオプションがあり、依存関係を調べてツリー表示できます。
ここでいう依存関係は`Requires=`や`Wants=`といった、必要となるunitに着目した依存関係です。

起動順に着目して調べるには、`list-dependencies`に`--before`や`--after`を併せて指定します。

`--before`を指定するとunitファイルの`Before=`ディレクティブをたどって依存関係を表示します。
同様に`--after`を指定するとunitファイルの`After=`ディレクティブをたどって依存関係を表示します。

例えば、`getty@tty1`がどのサービスの前に起動していないといけないことになっているかを調べるには次のようにします。

```sh
% systemctl list-dependencies --before getty@tty1
getty@tty1.service
● ├─getty.target
● │ └─multi-user.target
● │   ├─systemd-update-utmp-runlevel.service
● │   └─graphical.target
● │     └─systemd-update-utmp-runlevel.service
● └─shutdown.target
```


これは、実際に`getty@tty1.service`に以下の記述があることと一致しています。

```text
# If additional gettys are spawned during boot then we should make
# sure that this is synchronized before getty.target, even though
# getty.target didn't actually pull it in.
Before=getty.target
IgnoreOnIsolate=yes
```


### まとめ

Systemdのサービスの依存関係を調べる方法として、`systemd-analyze`と`systemctl`の使い方を紹介しました。
Systemdで依存関係にまつわるトラブルに遭遇したら、使ってみると便利かも知れません。

[^0]: デフォルトでtimeオプションを指定したのと同じ挙動になるため。
