---
tags:
  - apache-arrow
  - presentation
title: 'db tech showcase ONLINE 2020 - Apache Arrowフォーマットはなぜ速いのか #dbts2020'
---
[db tech showcase ONLINE 2020](https://www.db-tech-showcase.com/dbts/2020/online/)の12月8日（明日！） 15:30-16:10のセッションで「Apache Arrowフォーマットはなぜ速いのか」という話をする須藤です。まだ登録できるのでApache Arrowフォーマットに興味がある人はぜひこのセッションに参加してください！セッション中はチャットで私と質疑応答できます！
<!--more-->


<div class="youtube">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/_Gqxu212NFw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-online-2020/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-online-2020/" title="Apache Arrowフォーマットはなぜ速いのか">Apache Arrowフォーマットはなぜ速いのか</a>
  </div>
</div>


関連リンク：

  * [動画（YouTube）](https://www.youtube.com/watch?v=_Gqxu212NFw)

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/db-tech-showcase-online-2020/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/db-tech-showcase-online-2020)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-db-tech-showcase-online-2020)

### 内容

db tech showcaseで話すのは[2年ぶり]({% post_url 2018-09-20-index %})です。前はGroonga関連の話をしていましたが、今回はApache Arrowの話をします。

最近は[SciPy Japan 2020でApache Arrowを知らない人向けにApache Arrowを紹介]({% post_url 2020-09-24-index %})しました。これまでもApache Arrowを知らない人向けに説明していたのですが、このときは少し趣向を変えてまとめました。狙いがうまくいったかを確認するために広く感想を求めたこともあって、たくさんフィードバックをもらえました。このまとめ方でわかったことは、このまとめ方のほうが従来の説明よりも伝わりやすいが、説明する私はあまり楽しくないということでした。Apache Arrowが広い領域をカバーしていることもあって、Apache Arrowの全体像を説明しようとすると広く浅い説明になっていました。1回2回広く浅い説明をするのは大丈夫なのですが、何度も広く浅い説明をしていたところ、私はだんだん説明することがつまらなくなってきていました！なんと！

ということで、今回からApache Arrowの紹介方法を変えました。Apache Arrowの全体像を説明することをやめ、特定の領域に絞って深く説明することにしました。おそらく、これからもApache Arrowを説明する機会は何度もあるはずなので、それぞれの機会ごとに違う領域を深く説明する予定です。それぞれの私の説明を聞いてもApache Arrowの全体像をつかめないと思いますが、全体像を知りたい人には、一連の私の説明を聞くか、SciPy Japan 2020のように過去に全体像を説明したときの情報を使ってもらおうと割り切りました。これでこれからも私は楽しくApache Arrowの説明をできるはず！

最初は現時点で一番よく使うだろう「Apache Arrowフォーマット」に焦点を絞って速さの秘密を説明しています。詳細は前述の動画やスライドを参照してください。

### 動画作成方法

db tech showcase ONLINE 2020は事前録画した発表内容をストリーミングし、随時チャットで発表者と質疑応答するスタイルです。私はDebian GNU/Linux上で[Open Broadcaster Software](https://obsproject.com/)を使って録画しました。今回は動画編集にもチャレンジしました。[Shotcut](https://shotcut.org/)を使って、うまく説明できずに説明し直しているところを切り貼りしました。つなぎ部分がぎこちなく聞こえてしまうのですが今の私の編集力ではコレが限界でした。元の動画から10分くらい短くなり、以下にスムーズに説明できていないかを実感しました。。。

### まとめ

2020年12月8日（明日！） 15:30-16:10にdb tech showcase ONLINE 2020でApache Arrowフォーマットがなぜ速いのかを説明します。セッション中は私と質疑応答できるので、興味がある人はぜひセッションに参加してください！

都合があわないという人は前述の通りすでに動画・スライドを公開しているのでそれを参照してください。感想・質問は https://twitter.com/ktou でお待ちしています！

Apache Arrow関連の技術サポートが必要な場合は[お問い合わせ](/contact/)ください。
