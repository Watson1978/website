---
tags:
- ruby
- presentation
title: 'RubyKaigi Takeout 2020 - Goodbye fat gem #rubykaigi'
---
[RubyKaigi Takeout 2020](https://rubykaigi.org/2020-takeout)の1日目に[Goodbye fat gem](https://rubykaigi.org/2020-takeout/speakers#ktou)という話をする須藤です。今年もクリアコードは[スポンサー](https://rubykaigi.org/2020-takeout/sponsors#sponsor-200)としてRubyKaigiを応援します。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-takeout-2020/viewer.html"
	  width="640" height="404"
	  frameborder="0"
	  marginwidth="0"
	  marginheight="0"
	  scrolling="no"
	  style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
	  allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-takeout-2020/" title="Goodbye fat gem">Goodbye fat gem</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-takeout-2020/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/rubykaigitakeout2020goodbyefatgem)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-rubykaigi-takeout-2020)

### 内容

RubyKaigiではRuby関連の自分の活動を自慢しているのですが、今年はfat gemまわりの活動を自慢します。

fat gemまわりの状況は[2019年、fat gemをやめる]({% post_url 2019-11-22-index %})としてすでにまとめていて、今回の内容もこの記事の内容をベースにしています。ちなみに、私はRubyKaigiに応募する前に応募する内容を記事にして興味がある人はいそうかということを確認しています。fat gemの内容は少しは興味がある人はいそうで、去年の[RubyKaigi 2019のcsvの話]({% post_url 2019-04-20-index %})は[Ruby 2.6.0とより高速なcsv]({% post_url 2018-12-25-index %})の反響をみる感じではそれなりにいそうで、[RubyKaigi 2015のテスティングフレームワークの話]({% post_url 2015-12-12-index %})は[Rubyのテスティングフレームワークの歴史（2014年版）]({% post_url 2014-11-06-index %})の反響をみる感じではけっこういそうでした。

fat gemは私自身がかなり活用していたこともあり、[rake-compiler](https://github.com/rake-compiler/rake-compiler)などfat gem関連のツールのメンテナンスをしています。しかし、[RubyInstaller for Windows](https://rubyinstaller.org/)の改良によりfat gemを使わなくてもよい状況になりつつあります。私が認識している最後の課題は[Bundlerにあった](https://github.com/rubygems/bundler/pull/7522)のですが、それも私が直しておきました。（どーん！けっこう難しかったんだよ！）Bundler 2.2.0がリリースされればfat gemを使わなくてもよい状況になるはずです。

ただ、この状況を知らずにfat gemを提供しつづけているgem開発者はまだそれなりにいるように私にはみえます。そのため、RubyKaigiの場でfat gem関連の最新情報を共有して、fat gemを提供しつづけるかどうか考える機会にして欲しいと思っています。fat gemをやめられればfat gemのメンテナンスコスト（それなりにあるはず）がなくなり、メンテナンスのためのリソースを別のことに使えます。これはgem開発者にとってよいことのはずです。

fat gemを提供しているgem開発者に届くといいなぁ。

### 動画作成方法

RubyKaigi Takeout 2020は事前録画した発表内容をストリーミングするスタイルです。私はDebian GNU/Linux上で[Open Broadcaster Software](https://obsproject.com/)を使って録画しました。[Shotcut](https://shotcut.org/)を使って音量を調整したりとか動画編集もしてみたかったのですが、時間の関係でできませんでした。[クリアコードのYouTubeチャンネル](https://www.youtube.com/channel/UCkEC-Cwkvjctie6TdL6ykxg)を作ってみたので、動画編集できたらアップロードしようと思っています。

2020年10月30日には[SciPy Japan 2020でApache Arrowの紹介](https://www.scipyjapan.scipy.org/schedule)をする予定で、それも事前録画なので、そのときは動画編集もしてみたいです。

### まとめ

fat gemを提供しているgem開発者はぜひ聴いてみてね。

私がBundlerを直したように「手元で問題を回避するよりも問題が発生している原因を直すスタイル」で仕事をしたい人はクリアコードという会社がオススメなので[会社説明会に応募](/recruitment/)してね。
