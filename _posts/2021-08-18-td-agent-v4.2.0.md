---
tags:
- fluentd
title: TD Agent v4.2.0をリリース
author: kenhys
---

TD Agent v4.2.0をリリースしました。

クリアコードは[Fluentd](https://www.fluentd.org)の開発に参加しています。
TD Agent v4.2.0のリリースはクリアコードがメンテナンス作業の一環として行いました。

今回は、v4.2.0の変更点をFluentd Blogの[TD Agent v4.2.0 has been released](https://www.fluentd.org/blog/td-agent-v4.2.0-has-been-released)記事をもとに紹介します。

なお変更点の詳細についてはv4.2.0の[CHANGELOG](https://github.com/fluent-plugins-nursery/td-agent-builder/blob/master/CHANGELOG.md#release-v420---20210729)を参照してください。

<!--more-->

### バンドルしているFluentdを1.13.3に更新しました

TD Agent 4.1.1では1.12.3をバンドルしていましたが、TD Agent 4.2.0では最新の1.13.3をバンドルするようにしました。

`in_tail`プラグインの不具合を数多く修正しています。

概要を知るにはこれまでの各リリースごとの記事を参照することをおすすめします。

* https://www.fluentd.org/blog/fluentd-v1.12.4-has-been-released
* https://www.fluentd.org/blog/fluentd-v1.13.0-has-been-released
* https://www.fluentd.org/blog/fluentd-v1.13.1-has-been-released
* https://www.fluentd.org/blog/fluentd-v1.13.2-has-been-released
* https://www.fluentd.org/blog/fluentd-v1.13.3-has-been-released

Fluentdの変更点の詳細については[CHANGELOG.md](https://github.com/fluent/fluentd/blob/master/CHANGELOG.md#v1133)を参照してください。

### バンドルしているRubyを2.7.4に更新しました

Rubyにバンドルされているresolv gemによりCPU負荷が高くなる現象がありましたが、この問題がRuby 2.7.4では解決しています。

詳細は https://github.com/fluent/fluentd/issues/3382 を参照してください。

### jemalloc をv3.6.0にダウングレードしました

これまでメモリ管理のライブラリーとしてjemallocのv5.2.1を使用していました。
しかし、v5.xはRubyのメモリ管理とあまり相性がよくないため、よりメモリ使用量が少なくてすむv3.6.0に戻すことにしました。
v3.xは安定版のリリースでバグフィックスのみですが、まだメンテナンスはされています。

なお、今回jemallocをダウングレードしましたが、これが唯一の解決方法というわけではありません。
他にもメモリ消費量を削減する方法はいくつか議論されています。したがって、今後よりよい方法を模索していきます。

### Perlへのパッケージの依存をなくしました

これまでjemalloc由来の`jeprof`をパッケージにバンドルしていました。
しかし、`jeprof`を使わないのに、依存があるせいでPerl関係のパッケージを数多くインストールしないといけない状態でした。

不必要なパッケージをできるだけインストールしたくないという要望があったので、v4.2.0では`jeprof`のバンドルをやめ、Perlの依存関係で余計なパッケージをインストールしなくてもよいようにしました。

### RPMパッケージのリポジトリの署名に対応しました

従来、RPM自体には署名をして配布していたのですが、リポジトリについては署名をしていませんでした。
今回からリポジトリへの署名をはじめました。

`/etc/yum.repos.d/td.repo`に`repo_gpgcheck=1`を追記すると、リポジトリの署名をチェックするようになります。
インストールされるRPMパッケージが正規のリポジトリから配布され、改竄されていないことを確認できます。

ドキュメントの[インストール手順](https://docs.fluentd.org/installation/install-by-rpm)に記載のスクリプトにも上記の設定がそのうち有効になるはずです。

### その他

TD Agent v4.2.0では、ほかにもバンドルしているgemを更新したりもしています。

### さいごに

クリアコードでは[Fluentd/Fluent Bitのサポートサービス]({% link services/fluentd-service.md %})を提供しています。
また、[Fluentd/Fluent Bitに携わりたい開発者も募集しています]({% link recruitment/index.md %})。
こちらの[お問い合わせフォーム]({% link contact/index.md %})からご連絡ください。
