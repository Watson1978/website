---
tags:
- fluentd
title: 'OSS開発支援の一例: Fluentdとその関連ソフトウェア'
---
クリアコードでは[OSS開発支援サービス]({% post_url 2016-06-27-index %})の一環で[TreasureDataさん](https://www.treasuredata.com/jp/)が中心になって開発し公開している[Fluentd](http://www.fluentd.org/)とそのプラグインなど[^0]の開発を支援しています。
かれこれ一年ほどやってきたので、どのようなことをやってきたのかふりかえります。
<!--more-->


TreasureDataさんの目的はFluentdの普及促進です。Fluentdが普及すると、TreasureDataさんのサービスを利用する人が増えるためです。

クリアコードはTreasureDataさんの目的を達成するために以下のことをやっています。

  * TreasureData さんが手の回っていないところをサポートする

    * IssueやPRに対応する

    * メンテナのいないプロダクトでよく使われているものはメンテナンスを引き取る

    * 新機能の開発

    * バグの修正

    * その他、Fluentdの普及促進につながること

  * Fluentd やその関連ソフトウェアに関する技術情報を発信する

    * ククログやQiita等に記事を書く

直接依頼されたことだけでなく、Fluentdの普及促進につながることを積極的にやっています。

なお、TreasureDataさんとは基本的にはIssue/PullRequestでやりとりしています。作業の優先順位やIssue/PullRequestで相談しづらいことを相談するときはSlackを使用しています。

### IssueやPRに対応する

IssueやPRにコメントを書くことによって、コミュニティが活発になります。コミュニティが活発になると次のような理由で新規ユーザーを惹き付ける要因の一つになります。よってFluentdの普及促進につながります。

  * 今後の発展を期待できる

  * 問題が見つかりやすくなり安定化につながる

  * ユーザー間での情報共有が活発になり早く問題解決できる

コメントを書いたIssueやPRのうち特に問題の解決につながったものをリストアップしました。

  * [Add enable_fallback option by cosmo0920 · Pull Request #37 · fluent/fluent-plugin-sql](https://github.com/fluent/fluent-plugin-sql/pull/37)

  * [EventEmitter memory leak · Issue #43 · fluent/fluent-logger-node](https://github.com/fluent/fluent-logger-node/issues/43)

  * [Add support for Unix domain socket. by masahide · Pull Request #17 · fluent/fluent-logger-golang](https://github.com/fluent/fluent-logger-golang/pull/17)

### メンテナのいないプロダクトでよく使われているものはメンテナンスを引き取る

これらは依頼されて引き取ったものもありますが、Fluentdが内部で利用しているライブラリもあります。
こういったプロジェクトを引き取りメンテナンスを継続することによって、既存ユーザーは安心して使い続けることができますし、新規ユーザーも安心して使い始められます。安心して使えていると既存ユーザーは他の人から意見を求められたときに「安心して使える」という情報を伝えてくれます。このようにFluentdの普及促進につながります。

  * [kiyoto/fluent-plugin-grok-parser: Fluentd's Grok parser](https://github.com/kiyoto/fluent-plugin-grok-parser)

  * [fluent/fluent-logger-node: A structured logger for Fluentd (Node.js)](https://github.com/fluent/fluent-logger-node)

  * [cosmo0920/windows-pr: A collection of Windows functions, constants and macros predefined for you for win32-api](https://github.com/cosmo0920/windows-pr)

  * [cosmo0920/windows-api: A wrapper for win32-api that simplifies and automates common idioms](https://github.com/cosmo0920/windows-api)

  * [cosmo0920/win32-api: A different, better variant of the Win32API Ruby library](https://github.com/cosmo0920/win32-api)

### 新機能の開発

既存のFluentdのままでもかなり便利ですが、以下の点をより強化することによりさらにFluentdの普及促進につながります。

  * 機能追加

    * Fluentdを活用できるケースが増えて普及促進につながります。

  * 既存機能の改良

    * 運用中のFluentdのメンテナンスが楽になります。運用が楽になると既存ユーザーはよりFluentdを活用でき、まわりにその情報を提供してくれる可能性が増えます。また、新しくFluentdを活用できるケースが増えて普及促進につながります。

  * テスト周りの改良

    * プラグインを開発しやすくなり、既存プラグインのメンテナンス・新規プラグインの開発がはかどります。プラグインが増えるとFluentdを活用できるケースが増えて普及促進につながります。

依頼されたものだけでなく、特にFluentdの普及推進につながるものをリストアップしました。

  * 以前ククログでも紹介しましたがFluentdに`--show-plugin-config`オプションを追加しました

    * [Add show plugin config by okkez · Pull Request #663 · fluent/fluentd](https://github.com/fluent/fluentd/pull/663)

  * fluent-plugin-s3でAWS SDKv2を使うようにしました

    * [Use aws sdk v2 by okkez · Pull Request #85 · fluent/fluent-plugin-s3](https://github.com/fluent/fluent-plugin-s3/pull/85)

  * fluent-plugin-webhdfsにHDFSでよく使われているSnappyという圧縮形式をサポートしました。そのためにRubyでSnappyを扱うためのライブラリにAPIの追加するためのPRも送りました。

    * [Add snappy compressor by okkez · Pull Request #40 · fluent/fluent-plugin-webhdfs](https://github.com/fluent/fluent-plugin-webhdfs/pull/40)

    * [Add #write and #close to Snappy::Writer by okkez · Pull Request #16 · miyucy/snappy](https://github.com/miyucy/snappy/pull/16)

  * [okkez/fluent-plugin-concat](https://github.com/okkez/fluent-plugin-concat)

    * Docker Fluentd logging driver 経由だと複数行のログが複数のイベントに分割されてしまうのを一つのイベントにまとめるプラグイン。Docker側で直すのが筋なのですが、現実に困っている人がいたので依頼されて開発しました。

  * in_tailのmultilineサポートに対応するための開発を行いました

    * [Support multiline by okkez · Pull Request #10 · kiyoto/fluent-plugin-grok-parser](https://github.com/kiyoto/fluent-plugin-grok-parser/pull/10)

  * 3rdパーティ製のFluentdのoutputプラグインにfilterプラグインを追加

    * [Add filter plugin by okkez · Pull Request #24 · y-ken/fluent-plugin-geoip](https://github.com/y-ken/fluent-plugin-geoip/pull/24)

    * [Add filter plugin by okkez · Pull Request #8 · y-ken/fluent-plugin-anonymizer](https://github.com/y-ken/fluent-plugin-anonymizer/pull/8)

    * [Add filter plugin by cosmo0920 · Pull Request #24 · tagomoris/fluent-plugin-parser](https://github.com/tagomoris/fluent-plugin-parser/pull/24)

    * [Add filter plugin by cosmo0920 · Pull Request #4 · yu-yamada/fluent-plugin-add](https://github.com/yu-yamada/fluent-plugin-add/pull/4)

  * 3rdパーティ製のFluetndのoutputプラグインのissueに対応

    * [Add content_type option by okkez · Pull Request #31 · u-ichi/fluent-plugin-mail](https://github.com/u-ichi/fluent-plugin-mail/pull/31)

    * [delimeter problem with grepcounter plugin · Issue #17 · u-ichi/fluent-plugin-mail](https://github.com/u-ichi/fluent-plugin-mail/issues/17)

  * parserプラグイン用のv0.12向けのテストドライバーを追加しました

    * [Add ParserTestDriver by cosmo0920 · Pull Request #702 · fluent/fluentd](https://github.com/fluent/fluentd/pull/702)

  * formatterプラグイン用のv0.12向けのテストドライバーを追加しました

    * [Add FormatterTestDriver by cosmo0920 · Pull Request #712 · fluent/fluentd](https://github.com/fluent/fluentd/pull/712)

  * fluent-logger-golangでJSON形式のデータを送れるようにしました

    * [Add JSON marshaler by cosmo0920 · Pull Request #27 · fluent/fluent-logger-golang](https://github.com/fluent/fluent-logger-golang/pull/27)

  * Fluentdの起動時に表示されるログにプラグインのIDと名前を表示するようにしました

    * [Append plugin name and id to log by cosmo0920 · Pull Request #860 · fluent/fluentd](https://github.com/fluent/fluentd/pull/860)

  * in_dummyにsuspendオプションを追加しました

    * [Add suspend option to in dummy plugin by cosmo0920 · Pull Request #900 · fluent/fluentd](https://github.com/fluent/fluentd/pull/900)

  * fluent-plugin-sqlのout_sqlでfallbackの有効・無効を切り替えられるオプションを追加しました

    * [Add enable_fallback option by cosmo0920 · Pull Request #37 · fluent/fluent-plugin-sql](https://github.com/fluent/fluent-plugin-sql/pull/37)

### バグの修正

バグを修正することにより、それが原因で使えなかったケースでもFluentdを活用できるようになり、Fluentdの普及促進につながります。また、既存ユーザーは運用で回避せずに済むため、よりFluentdを楽に運用できるようになります。このような既存ユーザーが情報共有といった形でコミュニティで活躍してくれるとコミュニティがさらに活発になりFluendの普及促進につながります。

依頼されたものではなく、自分たちで見つけたバグを修正したものをリストアップします。

  * 削除された例外クラスを使用しないようにしました

    * [Stop using ActiveRecord::ThrowResult by okkez · Pull Request #32 · fluent/fluent-plugin-sql](https://github.com/fluent/fluent-plugin-sql/pull/32)

  * Rubyのバージョンによってテストが失敗するようになっていたのを修正しました

    * [Read dump data as binary by okkez · Pull Request #17 · repeatedly/fluent-plugin-netflow](https://github.com/repeatedly/fluent-plugin-netflow/pull/17)

  * in_httpのHTTPレスポンスヘッダーでRFCで推奨されている形式を使うようにしました

    * [In http: Use RFC suggested character cases of HTTP response header by cosmo0920 · Pull Request #836 · fluent/fluentd](https://github.com/fluent/fluentd/pull/836)

  * fluent-debug コマンドが動作しなくなっていたのを修正しました

    * [fluent-debug: Fix broken fluent-debug command by cosmo0920 · Pull Request #891 · fluent/fluentd](https://github.com/fluent/fluentd/pull/891)

### Fluentd関連の技術情報発信

QiitaやククログでFluentd関連の技術情報を発信しています。

  * [fluent-logger-node 2.0.1 - Qiita](http://qiita.com/okkez@github/items/8636cd3f9c3009262b78)

  * [ククログのFluentdカテゴリー](/blog/category/fluentd.html)

### その他、Fluentdの普及促進につながること

他にも以下のことを実施しました。

  * Fluentdのプラグインを新機能に対応させる[^1]

    * v0.12.11で追加された `secret` パラメータ対応

    * v0.12.16/v0.12.17で追加された `desc` 対応

    * v0.12.0で追加されたfilterプラグインに対応するための修正

  * Fluentd v0.14のプラグインAPIへの対応[^2]

  * ライセンス表記の修正

  * TravisCIの設定の追加・修正・更新

  * Rubyのバージョンアップに伴う修正

  * ユニットテストの追加

  * 警告の除去

  * [flume-ng-fluentd-sink](https://github.com/cosmo0920/flume-ng-fluentd-sink)の開発

  * [fluent-bit](https://github.com/fluent/fluent-bit)のHomebrewのFormulaの作成の[プルリクエスト](https://github.com/Homebrew/legacy-homebrew/pull/44075)

  * RustのFluent Loggerの[fruently](https://github.com/cosmo0920/fruently)の開発

これらは少し遠回しなものもありますが、次の理由でFluentdの普及促進につながると考えて実施しました。

  * Fluentdの新しいユーザーが簡単に新しいFluentdやFluentdの関連ソフトウェアを使えるようになるため

  * ライセンス表記を正しいものに修正すると、ユーザーがそのソフトウェアを使ってもよいものなのか簡単に正しく判断することができるようになるため

  * メンテナンス性を向上させて開発を続けやすくするため

  * Flume NGからFluentdへの移行や接続を容易にするため

  * Fluentdを動かすには厳しい環境に対してもFluentdのエコシステムに乗っかれるかどうかの検証環境の導入を容易にするため

  * 新しい言語のユーザーにFluentdへの関心を持ってもらえるようにするため

### まとめ

このようにクリアコードではOSSの開発支援を実施しているので、OSSの開発をしているけれど手が足りなくて思うように進められないといったことがあれば相談してみてください。
相談は[フォーム](/contact/?type=oss-development)からメッセージを送ってください。

[^0]: fluent-plugin-xxxやfluent-logger-xxxが多い

[^1]: 3rdパーティ製も含む

[^2]: 絶賛対応中
