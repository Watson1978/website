---
tags:
- mozilla
title: Firefox 67以降のユーザープロファイルの仕様の詳細
---
Firefox 67の新機能の1つに、[インストール先ごとにプロファイルが分けられるようになった](https://blog.nightly.mozilla.org/2019/01/14/moving-to-a-profile-per-install-architecture/)という物があります。
<!--more-->


この記事では、Firefoxのユーザープロファイルの仕組みの歴史的な経緯や、プロファイル切り替えの仕組み自体がどのように使われてきたかという点を踏まえながら、Firefox 67での新機能の性質を解説します。

### Firefoxの「ユーザープロファイル」

Firefoxのユーザープロファイルの仕組みは、元を辿ると、その前身となったNetscape Communicatorのユーザープロファイルの仕組みに行き着きます。

Netscape Communicatorが現役だった頃に広く使われていたWindows 95やWindows 98は、原則としてユーザーは1人だけであるという前提で設計されていたため、複数人で同じPCを共用する場合にユーザーを使い分けるという事ができませんでした。そのためNetscape Communicator独自の概念として「使用者ごとにユーザープロファイルを作り分けて、設定値やブックマークなどはその中で管理する」という仕組みが用意されました。

[![（図：Netscape Communicatorのユーザープロファイルをユーザーごとに使い分ける様子）]({{ "/images/blog/20190614_0.png" | relative_url }} "（図：Netscape Communicatorのユーザープロファイルをユーザーごとに使い分ける様子）")]({{ "/images/blog/20190614_0.png" | relative_url }})

その一方で、Windows XP以降のバージョン[^0]ではOS自体が複数ユーザーを想定した設計になっています。アプリケーション側で個別にユーザープロファイルを管理する必要は無く、アプリケーションの設定を使用者ごとに使い分けたい場合、単純にWindowsのユーザーを使い分ければよいという事になっています。

[![（図：Windowsのプロファイルをユーザーごとに使い分ける様子）]({{ "/images/blog/20190614_1.png" | relative_url }} "（図：Windowsのプロファイルをユーザーごとに使い分ける様子）")]({{ "/images/blog/20190614_1.png" | relative_url }})

以上のような歴史的な経緯があり、Firefoxのユーザープロファイルは「Windowsのユーザープロファイルごとに、複数のFirefoxのプロファイルが存在しうる」という少々複雑な状況となっています。

[![（図：Windowsのプロファイル上に複数のFirefoxのプロファイルが存在する様子）]({{ "/images/blog/20190614_2.png" | relative_url }} "（図：Windowsのプロファイル上に複数のFirefoxのプロファイルが存在する様子）")]({{ "/images/blog/20190614_2.png" | relative_url }})

実際には、Windowsのユーザーを使い分ければよい現状においては、使用者ごとにFirefoxのプロファイルを使い分けないといけないという状況はまず発生しません。よって、Firefoxのユーザープロファイルは今では*1人のユーザーが、「仕事用」「開発用」「趣味用」といった何らかの用途ごとに*使い分けるという用途が主流になっています。

[![（図：Windowsのプロファイル上で、用途ごとにFirefoxのプロファイルが使い分けられている様子）]({{ "/images/blog/20190614_3.png" | relative_url }} "（図：Windowsのプロファイル上で、用途ごとにFirefoxのプロファイルが使い分けられている様子）")]({{ "/images/blog/20190614_3.png" | relative_url }})

### 「ユーザープロファイルの使い分け」から「コンテキストやコンテナーの使い分け」への変化

ただ、一般的なユーザーが普段使いにおいてFirefoxのユーザープロファイルを使い分けるということは、実際にはあまり無いものと思われます。その最大の理由は、運用の面倒さです。

Firefoxのユーザープロファイルを使い分けるには、

  * Firefoxの起動オプションに`-p`オプションを指定してプロファイルマネージャを起動し、そこでプロファイルを管理する。

  * `about:profiles`でプロファイルを管理する。

このいずれかの方法でプロファイルを作成し、さらに、

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p private`（個人的な使用）

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p work`（仕事での使用）

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p test`（検証用）

のように起動オプションを変えたショートカットを複数用意する必要があります。

また、これだけでは「個人用のFirefox」「仕事用のFirefox」を並行して起動することができません。というのも、Firefoxのプロセスが既に起動している場合、別のプロセスで新たにFirefoxを起動しようとすると、既に起動している方のプロセスの方にプロセスが吸収されてしまう（そちらでウィンドウやタブが開かれる）という結果になるからです。これを回避するためには、

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p private`

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p work -no-remote`

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p test -no-remote`

のように、普段使いの（URLなどの関連付けからFirefoxが起動された時に使う）プロファイルを除いて他のショートカットには`-no-remote`オプションを指定し、前述の「既存プロセスに吸収される」挙動を無効化する必要があります。

また別の方法として、プロファイルマネージャを使用せずに以下のようにする方法もあります。

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -profile "C:\Users\username\private-profile" -no-remote`

`-profile`オプションで任意のフォルダをフルパスで指定すると、*プロファイルマネージャの管理下にないフォルダであっても、それをプロファイルとしてFirefoxを起動できます*。実験や検証のためのプロファイルをいくつも併用したり使い捨てにしたりという使い方をしたい場合、プロファイルマネージャを使わずともフォルダを用意するだけで済むため、この方法は非常に便利です。ただしこの場合も、`-no-remote`を付けないと「既に起動している方のプロセスの方にプロセスが吸収される」という動作になってしまう点には注意が必要です。

これだけ面倒な手順が必要なわりに、一般的なユーザーが普段使いで得られるユーザー体験はあまり良い物ではありません。というのも、実際の利用局面では「仕事中に見つけた面白そうな記事を個人用にブックマークしておく」「個人的な使用中に見つけた仕事に関係する記事を、仕事中に履歴から探す」といった場面が生じやすいのに対し、履歴やブックマークなどがプロファイルごとに別々に保存されていると、そういった「用途をまたいだ情報の共有」ができないからです。

この問題の解決策として現在のFirefoxには、「1つのユーザープロファイルで、閲覧履歴やブックマークなどは共有しつつ、起動中の1つのプロセスの中でログイン状態やCookieなどの認証情報は分離する」という事を可能にする、以下の機能が備わっています。

  * プライベートウィンドウ（そのウィンドウの中で閲覧したページには、通常モードで閲覧した時のCookieなどが通知されない）

  * [コンテナータブ](https://addons.mozilla.org/firefox/addon/multi-account-containers/)（用途ごとに認証情報を切り替える）

これらの機能は、「諸々の不便を被ってでも厳密にプロファイルを分けたい程ではない」「しかし、ログイン状態やCookieなどの認証情報は用途ごとに切り替えたい」という、実際の使用局面でよく見られるニーズに応えるものとなっています。

[![（図：コンテナーを使い分けている様子）]({{ "/images/blog/20190614_4.png" | relative_url }} "（図：コンテナーを使い分けている様子）")]({{ "/images/blog/20190614_4.png" | relative_url }})

ユーザープロファイルの使い分けよりもはるかに手軽に使えるため、*これらの機能で充分に用を果たせる場面では、ユーザープロファイルの使い分けのニーズはほぼ無くなった*と言ってよいでしょう。

### それでも残った「ユーザープロファイルを使い分ける必要がある場面」

以上のような経緯から、現在のFirefoxで「ユーザープロファイルを使い分けなければその目的を達成できない」場面は非常に限られています。その代表的な例が、*複数バージョンのFirefoxの並行起動*です。

[![（図：複数バージョンのFirefoxを専用プロファイルで使い分けている様子）]({{ "/images/blog/20190614_5.png" | relative_url }} "（図：複数バージョンのFirefoxを専用プロファイルで使い分けている様子）")]({{ "/images/blog/20190614_5.png" | relative_url }})

例えば、前述の手順を用いて複数バージョンのFirefoxを並行起動する場合、以下のように起動オプションでプロファイルを指定する事になります。

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p default`（64bit版Firefox）

  * `"C:\Program Files (x86)\Mozilla Firefox\firefox.exe" -p 32bit -no-remote`（32bit版Firefox）

  * `"C:\Program Files\Mozilla Firefox Beta\firefox.exe" -p beta -no-remote`（ベータ版）

Firefox 67からの変更は、まさにこの用途のためのものです。言い換えると、*Firefox 67以降のバージョンでは、「インストール先が異なるFirefoxの実行ファイルごとに、専用のユーザープロファイルを使用する」使い方が、特にユーザー側での工夫無しに自動的に行われるようになっている*という事になります。

### Firefoxが自身のインストール先を識別する処理の流れ

では、このような挙動は一体どのようにして実現されているのでしょうか。特に、「特定のインストール先のFirefox」と「特定のユーザープロファイル」とはどのように紐付けられているのでしょうか。鍵になるのは、インストール先のパスに基づく[City Hash](https://ctrlshift.hatenadiary.org/entry/20110415/1302869740)値です。

Firefox 67以降のバージョンでは、Firefoxは[プロセスの起動時](https://searchfox.org/mozilla-central/rev/ee806041c6f76cc33aa3c9869107ca87cb3de371/toolkit/profile/nsToolkitProfileService.cpp#766)に、[自身の実行ファイルが置かれたフォルダの位置](https://searchfox.org/mozilla-central/rev/ee806041c6f76cc33aa3c9869107ca87cb3de371/toolkit/xre/nsXREDirProvider.cpp#1187)から[City Hashのアルゴリズムに基づいてハッシュ値を求めます](https://searchfox.org/mozilla-central/rev/ee806041c6f76cc33aa3c9869107ca87cb3de371/toolkit/mozapps/update/common/commonupdatedir.cpp#401)。例えばFirefoxが`C:\Program Files\Mozilla Firefox`にインストールされていた場合、値は`308046B0AF4A39CB`のようになります。内部的にはこれは「install hash」と名付けられている模様です。

次にFirefoxは、ユーザープロファイルが置かれている`%AppData%\Mozilla\Firefox`（具体的には `C:\Users\ユーザー名\Appdata\Roaming\Mozilla\Firefox`）の位置にある`profiles.ini`から`[Install(install hash)]`という名前のセクションを探します。これは通常、以下のような内容になっています[^1]。

```
; こちらは従来からあるセクション
[Profile0]
Name=default
IsRelative=1
Path=Profiles/xxxxx.default

; 自動的に追加されたセクション
[Install308046B0AF4A39CB]
Default=Profiles/xxxxx.default
Locked=1
```


この`Default`の値が`profiles.ini`から見た相対パスとなっており、このパスを解決することで、特定のインストール先のFirefoxに対して紐付けられたプロファイルフォルダを特定できるというわけです。この仕組みがあるお陰で、関連付け等からFirefoxが起動された場合などで起動オプションでプロファイルが明示されていなくても、適切なプロファイルが自動的に選択されるようになっています。

ただし、同じインストール先であっても、[起動時のパスの指定の仕方によってはプロファイルが意図せず切り替わってしまう場合があります（別記事にて詳細な解説）]({% post_url 2019-07-05-index %})。Firefoxのパスを明示的に指定する場面では、必ず*正式なパス表記*を使うように注意して下さい。

なお、この機能について「Firefoxのバージョンごとにプロファイルが別れる」というような説明がなされている場合がありますが、以上の仕組みを見て分かる通り、*プロファイルはあくまでFirefoxのインストール先のパスに紐付いています*。インストール済みのFirefoxに別のバージョンのFirefoxを上書きインストールしたり、Firefox自身の自動更新機能で新しいバージョンに更新したりした場合であっても、*Firefoxのインストール先が変わらなければ、プロファイルは以前の物が引き続き使われます*。「Firefoxを更新したらその度に新しいプロファイルが作られる」という事はありません。（逆に言うと、この機能を使ってプロファイルを使い分けるためには、*必ずそれぞれのバージョンのFirefoxを別々の位置にインストールする必要があります*。）

### Windowsのレジストリ上での取り扱い

上記のinstall hashはWindowsのレジストリ上でも使われています。

従来は、Windowsのレジストリ上はFirefoxはあくまで「Firefox」という名前の1つのアプリケーションとして扱われており、複数のバージョンを異なるフォルダにインストールした場合は最後にインストールした物（最後にインストーラがレジストリに書き込みを行ったバージョン）がその代表として扱われるようになっていました。例えばシステム標準のブラウザの選択肢の項目は

  * `HKEY_LOCAL_MACHINE\SOFTWARE\Clients\StartMenuInternet\Firefox`

  * `HKEY_CURRENT_USER\Software\Clients\StartMenuInternet\Firefox`

とその配下に記録された情報に基づいて表示されていました。

Firefox 67以降のバージョンでは、この情報がインストール先ごとに書き込まれるようになっています。

  * `HKEY_LOCAL_MACHINE\SOFTWARE\Clients\StartMenuInternet\Firefox-(install hash)`

  * `HKEY_CURRENT_USER\Software\Clients\StartMenuInternet\Firefox-(install hash)`

同様に、関連付け対象のファイルやデータの型も

  * `HKEY_CLASSES_ROOT\FirefoxHTML-(install hash)`

  * `HKEY_CLASSES_ROOT\FirefoxURL-(install hash)`

のようにそれぞれインストール先ごとに登録されるようになりました。このように、Windowsのレジストリ上は*インストール先ごとのFirefoxはそれぞれ別々のアプリケーションとして認識されています*。

### アプリケーションの一覧上でそれぞれのインストール先を識別できない問題の回避策

以上の通り、Windowsのシステム上は各インストール先のFirefoxは別々の物として認識されているわけですが、ここで1つ問題があります。内部的にはそれぞれ別々のinstall hashを伴って認識されているものの、表示されるアプリケーション名としてはどれも「Firefox」となるため、標準のブラウザや関連付けの対象を選択する場面で*同じ名前の項目がいくつも並んでしまう*という事が起こってしまうのです。

[![（複数のバージョンのFirefoxが同じ名前で並んでいる様子）]({{ "/images/blog/20190614_6.png" | relative_url }} "（複数のバージョンのFirefoxが同じ名前で並んでいる様子）")]({{ "/images/blog/20190614_6.png" | relative_url }})

現在の所、この問題を解決するには手動でのレジストリの編集が必要です。この一覧に表示されるアプリケーション名は `HKEY_CLASSES_ROOT\Local Settings\Software\Microsoft\Windows\Shell\MuiCache` というキーの値が参照されており、

  * 値 `C:\Program Files\Mozilla Firefox\firefox.exe.FriendlyAppName` のデータ→`Firefox Release`

  * 値 `C:\Program Files\Mozilla Firefox ESR\firefox.exe.FriendlyAppName` のデータ→`Firefox ESR`

のように`実行ファイルのパス.FriendlyAppName`という名前の値のデータをそれぞれ編集しておくと、インストール先ごとの項目が分かりやすい名前で表示されるようになります。

[![（複数のバージョンのFirefoxにそれぞれ別の名前が付いている様子）]({{ "/images/blog/20190614_8.png" | relative_url }} "（複数のバージョンのFirefoxにそれぞれ別の名前が付いている様子）")]({{ "/images/blog/20190614_8.png" | relative_url }})

### 起動オプションの作用はどう変わったか

上記の通り、Firefox 67以降のバージョンは自分自身のインストール先に基づいて適切なプロファイルを自動的に選択するようになったわけですが、では、今まで使われていた「起動オプションでプロファイルを明示する」機能はどのような影響を受けたのでしょうか。

#### `-p プロファイル名`

前述した通り、Firefox 67以降のバージョンでは`%AppData%\Mozilla\Firefox\profiles.ini`に以下のように`[Install(install hash)]`という名前のセクションが追加されます。

```
; 従来からあるセクション
[Profile0]
Name=default
IsRelative=1
Path=Profiles/xxxxx.default

; 自動的に追加されたセクション
[Install308046B0AF4A39CB]
Default=Profiles/xxxxx.default
Locked=1
```


この時、2つのセクションで同じパスが参照されている点に注目して下さい。Firefoxは参照先のパスが一致する2つのセクションを関連付けて認識しています。そのため、*以下の2つのコマンド列は同等として扱われます*。

  * `"C:\Program Files\Mozilla Firefox\firefox.exe"` （install hashに基づいて自動的にプロファイルを選択）

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p default` （`-p`オプションでプロファイルを明示）

引数無しで起動したFirefoxのプロセスがある状態でプロファイルを明示してFirefoxを起動しても、逆に、プロファイルを明示して起動したプロセスがある状態で引数無しでFirefoxを起動しても、既に起動している方のプロセスに処理が吸収される（既存プロセスの方でウィンドウまたはタブが開かれる）という、従来の`-no-remote`オプション無しの動作と似た結果になります。

#### 同じバージョン・同じインストール先のFirefoxを、別プロセス・別プロファイルで起動する

別のプロファイルを指定して別のプロセスを並行動作させたい場合、従来は`-p 別のプロファイル名`と指定するだけでは不十分で、`-p 別のプロファイル名 -no-remote`と指定する必要がありました。Firefox 67以降のバージョンではこの点が改善され、*`-p 別のプロファイル名`と指定するだけで別プロセスのFirefoxを並行動作させられるようになりました*。

従来は`-no-remote`オプションが必須だったため、必然的に、これらのオプションを指定したショートカットからFirefoxを多重起動しようとすると、「既存プロセスに処理が吸収されない」「しかし、新しいプロセスで使おうとしているプロファイルは、別のプロセスによって既に使用中」ということで、「Firefoxは起動していますが応答しません。新しいウィンドウを開くには既存のFirefoxをプロセスを終了させなければなりません。」というメッセージが表示されてしまうという結果になっていました。Firefox 67以降では、この制限に煩わされる事はもうありません。

#### `-profile プロファイルのフルパス`

プロファイルをフルパスで指定して起動する場合も同様に、`-no-remote`オプション無しでの別プロセス起動が可能となりました。以下のように起動オプションを指定すると、それぞれのプロファイルでFirefoxのプロセスが並行して動作する状態になります。

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -profile "C:\Users\username\profile1"`

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -profile "C:\Users\username\profile2"`

この場合、同じ起動オプションでFirefoxを多重起動すると、既に起動していてそのプロファイルを使用しているプロセスに処理が吸収されます。

また、条件を整えると、*「起動オプションでプロファイル名を明示する場合」と「フルパスで明示する場合」を等価に扱う*ことや、あるいは、*「プロファイル名を明示」「フルパスで明示」「プロファイルを明示しない」の3つを等価に扱う*という事も可能になっています。

重要なのは以下の点です。

  * 当該プロファイルがプロファイルマネージャの管理下に入っている（`profiles.ini`にそのフォルダの情報が記載されている）。

  * 当該プロファイルのプロファイルマネージャ上の名前と、フォルダ名とが一致している。

以下は、`profiles.ini`を編集して上記の条件を満たすように設定したプロファイルの例です。

```
[Profile0]
;Name=default
Name=xxxxx.default
IsRelative=1
Path=Profiles/xxxxx.default
```


Firefoxのプロファイルマネージャはプロファイルを作成する際、表示用の名前の前にランダムな文字列を付与してフォルダを作成します[^2]。*この表示名が記載されている`Name=`の行を編集し、表示名がプロファイルのフォルダ名と一致する状態にします*。この状態であれば、以下の2つのコマンド列は等価に扱われるようになります。

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -profile "%AppData%\Mozilla\Firefox\Profiles\xxxxx.default"`

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p xxxxx.default`

また、install hashとの紐付けが

```
[Profile0]
Name=xxxxx.default
IsRelative=1
Path=Profiles/xxxxx.default

[Install308046B0AF4A39CB]
Default=Profiles/xxxxx.default
Locked=1
```


のように行われていれば、以下の3パターンがすべて等価に扱われるようになります。

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -profile "%AppData%\Mozilla\Firefox\Profiles\xxxxx.default"`

  * `"C:\Program Files\Mozilla Firefox\firefox.exe" -p xxxxx.default`

  * `"C:\Program Files\Mozilla Firefox\firefox.exe"`

ただ、前述した通り、*Firefox 67以降での新機能の恩恵を受けるためには、プロファイルをプロファイルマネージャの管理下に置く必要がある*、という点に注意が必要です。「フルパスでプロファイルを明示したい場面」と「プロファイル指定無しでプロファイルを自動認識させたい場面」が重なる事はあまりありませんが、この両方のニーズを同時に満たしたいという場合[^3]には、見落としが生じないようにくれぐれも気をつけて下さい。

### まとめ

以上、Firefox 67以降での「Firefoxのインストール先ごとのユーザープロファイルを使用する挙動」の詳細について解説しました。

Google Chromeが覇権を取った現在においては、Firefoxのニーズは一般ユーザーよりも（おそらくは検証用として）Web制作・Webアプリ開発を業としている人の方に偏っている模様です。「複数バージョンを並行して起動して検証する」という場面で特に有用なこの機能が導入されたのも、そのような背景があったからという事ではないかと考えられます。筆者ほど多くのバージョンを使い分ける事はそうそう無いと思われますが、通常リリース版・ベータ版・ESR版の3つを並行起動できるだけでも検証の負担は下がります。皆さんもこれを機に試してみてはいかがでしょうか。

また、今回の調査は現在Firefox ESR60を運用中のお客様にFirefox ESR68をご案内するにあたって、変更点の詳細を把握するために行いました。当社の法人向けFirefoxサポートでは、詳細な技術情報が提供されていない新機能の詳細の調査も有償にて承っております。「SIの業務において、お客さまの承認を得るために詳細なエビデンスの提出が求められているが、どこを対象に調査すればよいのか分からない」といった問題でお困りの方は、[メールフォームよりぜひお問い合わせ下さい。](https://www.clear-code.com/contact/)

[^0]: 正確には、Windows 2000やそれ以前のWindows NTなども含みます。

[^1]: Firefox 67以降のバージョンを初めて起動した時に、このような情報が追記されます。

[^2]: これは、ユーザープロファイルの位置を決め打ちで特定しにくいようにして、悪意の攻撃者の被害を受けにくくするための工夫です。

[^3]: 例えば、web-ext経由でFirefoxを起動して常用しているというような場合。
