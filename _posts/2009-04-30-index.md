---
tags:
- ruby
title: Ruby/groonga 0.0.1リリース
---
[データベース機能も備える全文検索エンジンgroonga](http://groonga.org/)をRubyから利用するための拡張ライブラリ[Ruby/groonga](http://groonga.rubyforge.org/)がリリースされました。
<!--more-->


Ruby/groongaはRubyGemsに対応しているので、以下のようにコマンド一発でインストールできます。（事前にmakeやgccやRubyのヘッダファイルなど拡張ライブラリのビルドに必要なソフトウェアを揃えておいてください。）

{% raw %}
```
% sudo gem install groonga
```
{% endraw %}

Ruby/groongaを利用するためには最新のgroonga 0.0.4が必要ですが、もし、システムにインストールされていない場合は自動的にダウンロードし、groongaのRubyGemsディレクトリの中にインストールします。この場合、最適化オプション（gccの-O2オプション）付きでビルドされますが、最適化オプション付きでgroongaをビルドすると、とても時間がかかります。（30分とか。）慌てずにのんびり待ってください。[^0]

### サンプル

Ruby/groongaでは、よりRubyらしい読み書きしやすいAPIでgroongaを利用できるようにすることを目的としています。例えば、groongaのテーブルはRubyのHashのように扱うことができます。また、テーブルやカラムの型に応じてRubyとgroonga上のデータを適切に変換することにより、特別なことを意識せずにgroongaのデータベース機能を使うことができます。

[Cで書かれたインデックスを自動更新するプログラム]({% post_url 2009-04-22-index %})をRubyで書くとこうなります。

{% raw %}
```ruby
#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require 'rubygems'
require 'groonga'

# 初期化
Groonga::Context.default_options = {:encoding => :utf8}
Groonga::Database.create

# テーブル定義
## <bookmarks>テーブルとそのカラムたちを定義
bookmarks = Groonga::Array.create(:name => "<bookmarks>")
bookmarks.define_column("uri", "<shorttext>")
bookmarks.define_column("comment", "<shorttext>")

## <lexicon>テーブルとそのカラムたちを定義
lexicon = Groonga::Hash.create(:name => "<lexicon>",
                               :key_type => "<shorttext>")
## MeCabで検索用単語を切り出す
lexicon.default_tokenizer = "<token:mecab>"
comment_index_column = lexicon.define_column("comment-index", bookmarks,
                                             :type => "index")

# ポイント: インデックス自動更新の設定
comment_index_column.source = "<bookmarks>.comment"

# ブックマークの登録: 3件
def add_bookmark(bookmarks, uri, comment)
  bookmark = bookmarks.add
  bookmark["uri"] = uri
  bookmark["comment"] = comment
end

add_bookmark(bookmarks,
             "http://groonga.org/",
             "an open-source fulltext search engine and column store")
add_bookmark(bookmarks,
             "http://qwik.jp/senna/",
             "an embeddable fulltext search engine")
add_bookmark(bookmarks,
             "http://cutter.sourceforge.net/",
             "a unit testing framework for C")


# 検索: 2回
def search(comment_index_column, word)
  result = comment_index_column.search(word)
  puts("search result: <#{word}>: #{result.size}")
  puts("uri\t\t\t | comment")
  result.each do |record|
    bookmark = record.key
    puts("#{bookmark['uri']}\t | #{bookmark['comment']}")
  end
  puts
end

search(comment_index_column, "search")
# 結果: <search>で検索したら2件ヒット
# search result: <search>: 2
# uri			 | comment
# http://groonga.org/	 | an open-source fulltext search engine and column store
# http://qwik.jp/senna/	 | an embeddable fulltext search engine

search(comment_index_column, "testing")
# 結果: <testing>で検索したら1件ヒット
# search result: <testing>: 1
# uri			 | comment
# http://cutter.sourceforge.net/	 | a unit testing framework for C
```
{% endraw %}

### 注意事項

Ruby/groongaには以下のような既知の問題があります。

  * RubyのGCでgroongaのオブジェクトを開放しすぎてしまう
  * それほど高速化のためのチューニングをしていない
  * リファレンスマニュアル・チュートリアルが半分位しか用意されていない
  * APIがまだ流動的

GCの問題は、groongaのgrn_ctxというメモリ管理機能を提供するオブジェクトとRubyがオブジェクトをどの順番でGCするかはわからないという動作のために起きています。これはgroonga本体とも協調しながら解決する予定です。ちなみに、通常のアプリケーションでこの問題が発生する可能性があるのはプロセスの終了時だけです。そのため、この問題のためにデータが壊れてしまうということはないと考えられます。

今回のリリースではよりRubyらしいAPIの提供の優先度を高くしたため、高速化のためのチューニングはそれほど行われていません。いくつかチューニング案があるので、それらを適用することにより、Rubyの読み書きしやすいAPIを利用しながらより高速な全文検索機能とデータベース機能を利用できるようになるでしょう。

ドキュメントが完備されていないのは、APIがまだ流動的なことやgroongaのすべての機能を網羅していないこととも関係があります。groonga本体もまだAPIが改良され続けています。それに追従したり、より使いやすいAPIを目指してRuby/groongaのAPIはこれから変更されるでしょう。その過程でドキュメントも充実していく予定です。

### 次のステップ

ラングバプロジェクトではRuby/groongaの開発に参加してくれる人を募集しています。興味のある方は[開発者向け情報](http://groonga.rubyforge.org/developer.html)をご覧ください。

クリアコードではRubyの拡張機能を書けるプログラミングが好きな開発者を募集しています。興味のある方は[採用情報](/recruitment/)をご覧ください。

[^0]: -O0オプション（非最適化オプション）をつけるとすぐにビルドできます。
