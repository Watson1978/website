---
tags: 
- embedded
title: Mac OS X版GTK+における日本語入力対応の近況
---
以前の記事で、Mac OS X版GTK+の日本語入力対応モジュール[GtkIMCocoa](https://github.com/ashie/gtkimcocoa)を開発中であることを紹介しました。
<!--more-->


今回は以下の最新のトピックについて紹介します。

  * Xamarin Studio用GtkIMCocoaバイナリのリリース
  * 最新版GTK+での日本語入力対応状況
  * 既知の不具合とその修正状況

### Xamarin Studio用GtkIMCocoaバイナリのリリース

[5月の記事]({% post_url 2013-05-23-index %})で、Xamarin Studioでの日本語入力がもう少しで可能になりそうだということを紹介しました。その後の調査で、GtkIMCocoaにいくつかのワークアラウンド[^0]を入れることで最低限の動作は可能であることを確認しました。これを機にGtkIMCocoaのバイナリモジュールをリリースしましたので、興味のある方は試して頂き、不具合等があれば[報告](https://github.com/ashie/gtkimcocoa/issues/new)して頂けると大変助かります。

![Xaramain Studioでの日本語入力の様子]({{ "/images/blog/20130703_0.png" | relative_url }} "Xaramain Studioでの日本語入力の様子")

なお、GtkIMCocoaは既に[Monoのビルドシステムに取り込まれて](https://github.com/mono/bockbuild/blob/master/packages/gtkimcocoa.py)います。また、新しいGTK+とともに後述するim-quartzも導入されるものと思われるため、今すぐ試したい・開発に協力したいということでなければ、慌てて本モジュールをインストールする必要はありません。今後リリースされるXamarin Studioではデフォルトで日本語入力が可能になるものと思われます。

#### 入手およびインストール方法

GtkIMCocoaのバイナリモジュールは以下のリンクからダウンロードして下さい。

  * [gtkimcocoa-0.0.2-gtk2-i386.zip](http://sourceforge.jp/projects/imime/downloads/59053/gtkimcocoa-0.0.2-i386-bin.zip/)

これに対応するソースコードは以下のリンクからダウンロードすることができます。

  * [gtkimcocoa-0.0.2.tar.gz](http://sourceforge.jp/projects/imime/downloads/59053/gtkimcocoa-0.0.2.tar.gz/)

GUIのインストーラは用意していません。インストールは本パッケージに含まれるシェルスクリプトをコンソールから実行することで行います。

{% raw %}
```
$ unzip gtkimcocoa-0.0.2-gtk2-i386.zip
$ cd gtkimcocoa-0.0.2-gtk2-i386
$ sudo ./install-for-xamarin.sh
```
{% endraw %}

すでにGtkIMCocoaがインストールされている場合は上書きされますので、ご注意下さい。また、後述するim-quartzがインストールされている場合は、im-quartz.so.bakにリネームされます。リネームされたim-quartzは、下記のコマンドでGtkIMCocoaをアンインストールすると元に戻されます。

{% raw %}
```
$ sudo ./install-for-xamarin.sh uninstall
```
{% endraw %}

上記スクリプトは、[Unity](http://japan.unity3d.com/)に付属するMonodevelopへのインストールには対応していません。ただし、インストール自体はim-cocoa.soをコピーしてGTK+の設定ファイルをgtk-query-immodules-2.0で更新するだけですので、興味のある方は上記スクリプトを参考に挑戦してみて下さい。

#### バグ報告方法

何か問題が見つかった場合は、[GitHubのissue](https://github.com/ashie/gtkimcocoa/issues/new)に登録して頂けると助かります。日本語・英語どちらでも構いません。

#### 既知の不具合

既知の不具合については後述します。

### 最新版GTK+での日本語入力対応状況

[3月の記事]({% post_url 2013-03-14-index %})で、GtkIMCocoaとは別の実装（im-quartz）[^1]がGNOMEのBTSに[報告されていること](https://bugzilla.gnome.org/show_bug.cgi?id=694273)を紹介しました。その後、正式にこの実装がGTK+に取り込まれてリリースされています。対応するGTK+のバージョンは以下になります。

  * [gtk+-2.24.19](http://ftp.gnome.org/pub/gnome/sources/gtk+/2.24/)
  * [gtk+-3.9.2（開発版）](http://ftp.gnome.org/pub/gnome/sources/gtk+/3.9/)

同BTSエントリにて、GtkIMCocoaはim-quartzの問題点を改善するアプローチであるということを主張していたのですが、いくつかすれ違いが重なってしまったこともあって、残念ながら現時点ではGtkIMCocoaのアプローチは採用されていません。何はともあれ、ひとまず標準のGTK+で日本語入力が可能となったことはよいニュースです。

ただし、[3月の記事]({% post_url 2013-03-14-index %})や[5月の記事]({% post_url 2013-05-23-index %})で紹介したGtkIMCocoaの不具合は、その後の調査でGTK+側のバグであったことが判明しています。つまり、im-quartzでも同様に問題が発生します。また、[3月の記事]({% post_url 2013-03-14-index %})で紹介した設計上の問題に起因するim-quartz固有の不具合が存在することも確認しています。これらの不具合の詳細については後述しますが、どちらの実装を利用するにしても、なるべくユーザーが不便を感じないように今後も改善していきます。

### 既知の不具合とその修正状況

現在のGtkIMCocoaやim-quartzには、ある程度詳細がわかっているものだけでも以下のような問題があります。

#### Google日本語入力使用時に反応が遅くなる不具合

[3月の記事]({% post_url 2013-03-14-index %})で紹介したGoogle日本語入力が動作しないという問題ですが、これは正確には「Google日本語入力等の一部のIMでキーをタイプすると反応が非常に遅くなり、また一部のキーは完全に動作しなくなる」という問題でした。その後の調査で、これはGTK+の問題であり、immoduleを無効にしていても発生する問題であることがわかりました。このため、GtkIMCocoaだけではなくim-quartzでも再現します。ただし、環境によっては問題が発生しないこともあるようです。

この問題についてはGTK+にパッチを提供済みで、既に修正は取り込まれています。gtk+-2.24.19には間に合いませんでしたが、2.24.20以降で適用されるものと思われます。GTK+ 3系については、3.9.6以降で適用されています。

  * [GNOME Bugzilla – Bug 698183](https://bugzilla.gnome.org/show_bug.cgi?id=698183)

#### 英数キーやかなキーでのモード切り替え時の挙動不具合

[5月の記事]({% post_url 2013-05-23-index %})で、Sylpheedにおいてキー操作による入力モード切り替えを行うと、フォーカスウィジェットが勝手に切り替わってしまう問題があることを紹介しました。その後の調査で、これはGTK+側で英数キーやかなキーをスペースキーとして認識していることによって発生している問題であることがわかりました。GTK+の問題であるため、GtkIMCocoaだけではなくim-quartzでも再現します。

この問題についてもGTK+にパッチを提供済みですが、まだGTK+開発者のレビューは受けていません[^2]。

  * [GNOME Bugzilla – Bug 702841](https://bugzilla.gnome.org/show_bug.cgi?id=702841)

今回リリースしたGtkIMCocoaのバイナリモジュールでは、ワークアラウンド処理を入れることでこの問題を回避しています[^3]。

#### Xamarin Studioで未確定文字列が表示されない

[5月の記事]({% post_url 2013-05-23-index %})で、Xamarin Studioにおいて未確定文字列が表示されない問題があるということを紹介しました。その後、[Xamarin Studio側で修正して頂きました](https://github.com/mono/monodevelop/commit/b2af5536d0c4271fac425105b3568f5cd51c7265)ので、今後リリースされるXamarin Studioではこの問題は解消されるはずです。また、今回リリースしたGtkIMCocoaのバイナリモジュールにはこの問題を回避するための処理が入っています[^4]ので、このモジュールを使用する場合は、修正が入っていないXamarin Studioでも日本語を入力することが可能です。

#### 変換中のカーソルキーの動作不具合（GtkIMCocoa・im-quartz）

カーソルキーでキャレットを未確定文字列[^5]の先頭より前あるいは最後尾より後に移動しようとすると、変換中文字列が消える問題があることを確認しています。GtkIMCocoa・im-quartz両方で再現します。これについては修正方法を検討中です。

#### かなテーブルや文字ビューアでの入力不具合（im-quartz）

im-quartzでは、かなテーブルや文字ビューアで文字を入力できないという問題あります[^6]。

この問題の存在自体は報告済みですが、[3月の記事]({% post_url 2013-03-14-index %})で紹介した根本的な設計上の問題点に起因している部分があるため、現在のim-quartzの設計を保ったままでの修正方法は提示できていません。

  * [GNOME Bugzilla – Bug 702843](https://bugzilla.gnome.org/show_bug.cgi?id=702843)

#### 英数キー2回押しによる英数変換の不具合（im-quartz）

Mac OS X標準の日本語入力システムである「ことえり」には、英数キーを2回連続で押すと、未確定文字列をアルファベットに戻して確定する機能があります。im-quartzでこの機能を使用すると、アルファベットは期待通り確定されるものの、未確定文字列が表示されたままになってしまう問題があります。この問題は上記のような設計の問題とは関係なく修正できる可能性があるため、修正方法を検討中です。

#### かなキー2回押しによるかな変換（GtkIMCocoa・im-quartz）

「ことえり」には、かなキーを2回連続で押すことで確定済み文字列を再変換する機能があります。GtkIMCocoaやim-quartzにはこの機能に対応するためのコードが実装されていません。GTK+側のAPIの制限のために他のプラットフォームでも問題になっている機能であるため、実装可能かどうかも含めて検討中です。

#### マルチディスプレイ環境で候補ウィンドウの位置がずれる（GtkIMCocoa）

GtkIMCocoaには、マルチディスプレイ環境で候補ウィンドウを出すと期待する位置に表示されない問題が存在します。im-quartzに同様の問題があるかどうかは確認はできていません。

### まとめ

今回の記事では、Xamarin Studio用のGtkIMCocoaをリリースしたことと、最新のMac OS X版GTK+ではデフォルトで日本語入力が可能となっていることを紹介しました。また、これらの実装の既知の不具合とその修正状況について紹介しました。ぜひ一度これらの実装を試して頂き、さらなる改善にご協力頂ければ幸いです。

GtkIMCocoaとim-quartzに共通の不具合を修正した場合は、GTK+プロジェクトに報告してim-quartz側にもマージしていくことを考えています。im-quartz固有のバグについても可能な限り直して行きたいと考えていますが、設計に起因するものについては修正が難しい可能性もあるので、引き続きGtkIMCocoaのようなアプローチによる実装も提案していきます。

[^0]: 正式な対応については、後述の通りGTK+本体やXamarin Studio側での修正が必要です。

[^1]: 3月の記事で紹介している通り、GtkIMCocoaはim-quartzにインスパイアされて開発を開始したものですが、根本的な設計が異なります。

[^2]: MonoのGTK+には先行して本パッチが[取り込まれています](https://github.com/mono/bockbuild/blob/master/packages/patches/gtk/bgo702841-fix-kana-eisu-keys.patch)。

[^3]: ソースからビルドする場合は、configureに--enable-workaroundオプションを追加することでこの処理を有効にできます。

[^4]: ソースからビルドする場合は、configureに--enable-workaroundオプションを追加することでこの処理を有効にできます。

[^5]: InputMethod用語ではコンポジション文字列あるいはプリエディット文字列などと呼ばれます。

[^6]: かなテーブルについては、正確には未確定文字列が表示されない、確定操作をすると逆に未確定文字列が現れて表示されたままになるという問題です。
