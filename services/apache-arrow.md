---
layout: default
main_title: Apache Arrowのご紹介
sub_title:
foot_note: Apache Arrow, the Apache Arrow project logo, Apache Spark, and Apache Parquet are either registered trademarks or trademarks of The Apache Software Foundation in the United States and other countries.
type: service
---

大容量のストレージ・大量のコンピューターリソースを活用することが可能になった現在では従来は現実的でなかった大量データを処理できるようになりました。現代のデータ処理システムは複数のデータ処理モジュールを連携して構築します。たとえば、[Fluent Bit](fluent-bit.html)で収集したログをAmazon S3に格納し、Amazon Athenaでデータを加工し、Amazon QuickSightで可視化した場合は4つのモジュールを連携しています。

各モジュール間ではデータ交換が発生します。大量のデータを処理するほどデータ交換の効率が重要になります。非効率なデータ交換処理をしていると、本来のデータ処理よりデータ交換処理の方に時間がかかっている状況になりかねません。

効率的なデータ交換のために設計されたデータフォーマットが[Apache Arrow](https://arrow.apache.org/)です。たとえば、従来はCSVやJSONを使っていたところをApache Arrowを使うようにすればデータ交換処理に必要なコンピューターリソースは非常に小さくなり、本来のデータ処理にコンピューターリソースを投入できます。

<div class="text-center">

![Apache Arrowでデータ交換を高速化](apache-arrow/data-exchange.png)

</div>

## Apache Arrowでの高速化事例

Apache Arrowフォーマットを使って高速化した事例を紹介します。

データ分析エンジンの1つである[Apache Spark™](https://spark.apache.org/)はApache Arrowフォーマットを使うことにより約30倍高速化しました。以下は[Apache Arrowの公式ブログで紹介されている事例](https://arrow.apache.org/blog/2017/07/26/spark-arrow/)の数値を可視化したものです。

<div class="text-center">

![Apache Spark™での高速化事例](apache-arrow/apache-spark-improvement.png)

</div>

## Apache Arrowの特徴

実はApache Arrowフォーマットによる高速化はApache Arrowが提供する機能の1つにすぎません。Apache Arrowはメモリー上でデータ処理するために必要な一連の機能を提供します。Apache Arrowフォーマットによるデータ交換の高速化以外にも次のような機能があります。

  * CPU上でSIMD・JITコンパイルを使って高速にデータを処理する機能
  * GPU上で高速にデータを処理する機能
  * CSVやApache Parquetなど既存のデータフォーマットと相互変換する機能
  * 高速なRPC機能

従来はこのような機能は各データ処理プロダクトで個別に実装されていましたが、Apache Arrowは各データ処理プロダクトで共有できる高速な実装を提供します。これにより各データ処理プロダクトはそれぞれの問題領域に注力できます。

## Apache Arrowの利用例

Apache Spark™をはじめApache Arrowを利用して高速化したデータ処理プロダクトが増えています。Apache ArrowのPythonライブラリーであるpyarrowだけでも、2020年8月23日の1日のダウンロード数は約23万件です。2020年7月の1.0.0リリースをきっかけにさらにApache Arrowの採用が進んでいます。

以下にApache Arrowの利用例を紹介します。

  * [Amazon Athenaの新しいフェデレーテッド・クエリによる複数データソースの検索 | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/query-any-data-source-with-amazon-athenas-new-federated-query/)：Amazon Athenaへのデータ提供時にApache Arrowを利用
  * [PostgreSQLだってビッグデータ処理したい！！～GPUとNVMEを駆使して毎秒10億レコードを処理する技術～ ](https://www.slideshare.net/kaigai/20191115pgconfjapan)：PostgreSQLで毎秒10億レコードを処理するためにApache Arrowを利用
  * [Vaex: A DataFrame with super strings | by Maarten Breddels | Towards Data Science](https://towardsdatascience.com/vaex-a-dataframe-with-super-strings-789b92e8d861)：Pythonのデータフレームの文字列処理の高速化にApache Arrowを利用
  * [Powered by | Apache Arrow](https://arrow.apache.org/powered_by/)：Apache Arrow公式サイトに集められた利用例集

## Apache Arrowの詳細

2018年よりApache Arrowの最新情報をまとめています。Apache Arrowの詳細はこちらの一連の記事を参照してください。

  * [Apache Arrowの最新情報（2022年5月版）]({% post_url 2022-05-13-latest-apache-arrow-information %})
  * [Apache Arrowの最新情報（2020年7月版）]({% post_url 2020-07-31-index %})
  * [Apache Arrowの最新情報（2019年9月版）]({% post_url 2019-09-30-index %})
  * [Apache Arrowの最新情報（2018年9月版）]({% post_url 2018-09-05-index %})

また、Apache Arrowの各特徴について個別に紹介した資料もあります。

  * [Apache Arrowフォーマットはなぜ速いのか]({% link downloads/apache-arrow-fast-format.md %})

## クリアコードとの関わり

クリアコードは2016年より継続的にApache Arrowの開発に参加しています。2020年8月24日時点で2番目に多くコミットした開発者（下図の緑枠）となっています。

クリアコードはApache Arrowについてコンサルティング・ソースコードレベルのサポートを提供している日本で唯一の企業です。

<div class="text-center">

![Apache Arrowのコミット数](apache-arrow/commit.png)

</div>

### サポートの問い合わせ

クリアコードではApache Arrowのサポートサービスを提供しています。自社のシステムをApache Arrowに対応させたい、Apache Arrowをどのように活用したらよいか相談に乗って欲しい、Apache Arrowを使っていて解決したい点があるなどの課題がございましたら[お問い合わせ](/contact/)からください。
