---
layout: default
main_title: データ収集ツールFluentd関連業務
sub_title:
type: recruit
---

クリアコードは2015年から[Fluentd](https://fluentd.org/)の開発に参加しており、現在ではFluentdやtd-agentのリリースも行っています。

業務内容は次のとおりです。

  * Fluentdを利用するお客様のサポート・コンサルティング
  * Fluentdの普及活動および案件獲得
  * Fluentdおよびtd-agentの開発・メンテナンス

## お客様のサポート・コンサルティング

Fluentdの導入を検討している・Fluentdの運用に関し課題をお持ちのお客様からご相談いただくことから始まります。

まず、お客様から課題やシステム構成などについてヒアリングを行います。
その結果に基づき、以下のような解決方法を検討、提案します。

  * 導入支援
  * 本体やプラグインへの機能追加・不具合修正
  * 新規プラグインの開発
  * 運用支援

チームで対応するため、1人ですべてに対応できなくても構いません。チームのメンバーと協力しながらお客様の課題を解決します。お客様に満足してもらった結果、同じお客様と長くお付き合いすることが多いです。お客様の課題に関する理解を深めながらよりよいシステムの構築を支援できます。

## 普及活動および案件獲得

クリアコードのサポートサービスをご利用されるお客様は、クリアコードが公開している情報をきっかけにご相談を頂き契約に至るケースが多いです。
Fluentdについても同様で、Fluentdおよび関連プロダクトの普及活動が案件獲得につながると考えています。
一方で、Fluentdは長くトレジャーデータ社が開発を主導されてきたため、現在では[クリアコードがメンテナンスを引き継いでいる]({% link press-releases/20210729-maintaining-fluentd.html %})という事実がまだ十分には周知されていないのが実情です。案件獲得の機会を増やすためには、クリアコードがより一層、主体的に情報発信をしていく必要があります。

具体的には次のような普及活動を行います。

  * 定期的なリリースおよびアナウンス
  * ブログでの情報発信
  * イベントでの発表
  * コミュニティー対応

普及活動は公開された場で行うため、あなたの実績としても広く参照できます。
また、関連コミュニティーでの人のつながりが増えます。人のつながりは公私問わず助けになります。

## Fluentdおよびtd-agentの開発・メンテナンス

ユーザーがFluentdを安心して使い続けられるよう、継続してFluentdおよびtd-agent、その他関連フリーソフトウェアの開発やメンテナンスを行います。

具体的な業務内容は次の通りです。

  * [Fluentd](https://github.com/fluent/fluentd)本体およびtd-agent（[fluent-pacakge-builder](https://github.com/fluent/fluent-package-builder/)）の開発
  * GitHub [fluent](https://github.com/fluent)および[fluent-plugins-nursery](https://github.com/fluent-plugins-nursery/)オーガニゼーション配下のFluentdプラグイン開発
  * [Fluentdウェブサイト](https://www.fluentd.org/)および[Fluentd公式ドキュメント](https://docs.fluentd.org/)のメンテナンス
  * その他関連フリーソフトウェアへの機能提案・バグ修正

Fluentdは世界中で広く利用されているため、日々様々なフィードバックが寄せられ非常にやりがいがあります。
興味を持たれた方はぜひ[会社説明会]({% link contact/index.md %})にご応募ください！
